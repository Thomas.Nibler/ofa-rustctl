# Rustctl: CTL at compile time in Rust's type system

This project implements Computation Tree Logic using Rust types. It allows you to compute CTL formulas
on Kripke structures and assert equalities between these terms, and the resulting program compiles
if and only if the asserted equalities hold.

### Why to use `rustctl`
         
Good question!

### How to use `rustctl`

See [main.rs](https://gitlab.unige.ch/Thomas.Nibler/ofa-rustctl/-/blob/main/src/main.rs?ref_type=heads) for a demo.

First, we define a Kripke structure. Each list of `Px` is a state, defined by the set of atomic propositions
satisfied by that state. `P4` is just an alias for the number 4 encoded in a type as the Peano number `Suc<Suc<Suc<Suc<Zero>>>>`.

```rust
type K = kripke!(
    (P1, P4) -> (P1, P2, P4),
    (P1, P2, P4) -> (P1, P4),
    (P5, P4) -> (P1, P4),

    (P3, P5) -> (P5, P4),
    (P3, P5) -> (P2, P5),
    (P2, P5) -> (P3, P5),

    (P6) -> (P1, P4),
    (P6) -> (P7),
    (P7) -> (P6),
);
```

Now we can define a set of states to use as an input to a CTL formula and an expected result.

```rust
type S1 = state_set!((P1));
type AGS1 = ctl!(K, AG(S1));

type ExpectedAGS1 = state_set!((P1, P4), (P1, P2, P4));
assert_type_eq!(AGS1, ExpectedAGS1);
```

This compiles only if the equality actually holds.

Nested CTL formulas also work:

```rust
type EFAGS1 = ctl!(K, EF(AG(S1)));

type ExpectedEFAGS1 = state_set!(
    (P1, P4),
    (P1, P2, P4),
    (P5, P4),
    (P3, P5),
    (P3, P5),
    (P2, P5),
    (P6),
    (P6),
    (P7),
);
assert_type_eq!(EFAGS1, ExpectedEFAGS1);
```

More details in the [project report](https://gitlab.unige.ch/Thomas.Nibler/outils-formels/-/blob/master/report.pdf?ref_type=heads).
