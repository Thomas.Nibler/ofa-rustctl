use std::marker::PhantomData;

use crate::boolean::{Bool, F, T};

use super::{Bottom, IsTerminal, Sfdd, Top};

pub trait BoolOp {
    type Res<A: Bool, B: Bool>: Bool;
}

pub struct And;
pub struct AndNot;
pub struct Implies;

pub struct _BoolOpImpl<A: Bool, B: Bool>(PhantomData<A>, PhantomData<B>);

impl BoolOp for And {
    type Res<A: Bool, B: Bool> = A::And<B>;
}

impl BoolOp for AndNot {
    type Res<A: Bool, B: Bool> = A::And<B::Not>;
}

impl BoolOp for Implies {
    type Res<A: Bool, B: Bool> = <<A as Bool>::Not as Bool>::Or<B>;
}

pub trait BoolToSfddTerm: Bool {
    type Res: Sfdd + IsTerminal;
}

impl BoolToSfddTerm for T {
    type Res = Top;
}

impl BoolToSfddTerm for F {
    type Res = Bottom;
}
