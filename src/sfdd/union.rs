use super::{helper::SfddHelp, Bottom, Comparable, Node, Sfdd, SfddLabel, Top};

pub trait Union<S: Sfdd> {
    type Res: Sfdd;
}

// Base cases
impl Union<Top> for Top {
    type Res = Top;
}

impl Union<Bottom> for Top {
    type Res = Top;
}

impl Union<Top> for Bottom {
    type Res = Top;
}

impl Union<Bottom> for Bottom {
    type Res = Bottom;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Union<Node<T, Take, Skip>> for Top
where
    Skip: Union<Top>,
{
    type Res = Node<T, Take, <Skip as Union<Top>>::Res>;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Union<Top> for Node<T, Take, Skip>
where
    Skip: Union<Top>,
{
    type Res = Node<T, Take, <Skip as Union<Top>>::Res>;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Union<Node<T, Take, Skip>> for Bottom {
    type Res = Node<T, Take, Skip>;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Union<Bottom> for Node<T, Take, Skip> {
    type Res = Node<T, Take, Skip>;
}

// Recursive case
impl<
        T1: Comparable<T2> + SfddLabel,
        Take1: Sfdd,
        Skip1: Sfdd,
        T2: Comparable<T1> + SfddLabel,
        Take2: Sfdd,
        Skip2: Sfdd,
    > Union<Node<T2, Take2, Skip2>> for Node<T1, Take1, Skip1>
where
    Skip1: Union<Node<T2, Take2, Skip2>>,
    Skip1: Union<Skip2>,
    Skip2: Union<Node<T1, Take1, Skip1>>,
    Take1: Union<Take2>,
    (
        Node<T1, Take1, <Skip1 as Union<Node<T2, Take2, Skip2>>>::Res>,
        Node<T1, <Take1 as Union<Take2>>::Res, <Skip1 as Union<Skip2>>::Res>,
        Node<T2, Take2, <Skip2 as Union<Node<T1, Take1, Skip1>>>::Res>,
    ): SfddHelp<T1, T2>,
{
    type Res = <(
        Node<T1, Take1, <Skip1 as Union<Node<T2, Take2, Skip2>>>::Res>,
        Node<T1, <Take1 as Union<Take2>>::Res, <Skip1 as Union<Skip2>>::Res>,
        Node<T2, Take2, <Skip2 as Union<Node<T1, Take1, Skip1>>>::Res>,
    ) as SfddHelp<T1, T2>>::Res;
}
