use crate::assert_type_eq;

use super::{helper::IfSfdd, Bottom, IsNode, Node, Sfdd, SfddEq, SfddLabel, Top};

pub trait Clean {
    type Res: Sfdd;
}

impl Clean for Top {
    type Res = Top;
}

impl Clean for Bottom {
    type Res = Bottom;
}

impl<T: SfddLabel, Skip: Sfdd + Clean> Clean for Node<T, Bottom, Skip> {
    type Res = <Skip as Clean>::Res;
}

impl<T: SfddLabel, Skip: Sfdd + Clean> Clean for Node<T, Top, Skip> {
    type Res = Node<T, Top, <Skip as Clean>::Res>;
}

impl<T: SfddLabel, Take: IsNode + Sfdd + Clean, Skip: Sfdd + Clean> Clean for Node<T, Take, Skip>
where
    <Take as Clean>::Res: SfddEq<Bottom>,
    (
        <Skip as Clean>::Res,
        Node<T, <Take as Clean>::Res, <Skip as Clean>::Res>,
    ): IfSfdd<<<Take as Clean>::Res as SfddEq<Bottom>>::Res>,
{
    type Res = <(
        <Skip as Clean>::Res,
        Node<T, <Take as Clean>::Res, <Skip as Clean>::Res>,
    ) as IfSfdd<<<Take as Clean>::Res as SfddEq<Bottom>>::Res>>::Res;
}

#[allow(dead_code)]
fn test_clean() {
    use crate::kripke_state::*;
    use crate::nat::*;
    type S1 = Node<Fr<P1>, Bottom, Bottom>;
    assert_type_eq!(<S1 as Clean>::Res, Bottom);

    type S2 = Node<Fr<P1>, Bottom, Top>;
    assert_type_eq!(<S2 as Clean>::Res, Top);

    type S3 = Node<Fr<P1>, Node<Fr<P2>, Bottom, Top>, Top>;
    assert_type_eq!(<S3 as Clean>::Res, Node<Fr<P1>, Top, Top>);

    type N4 = Node<Fr<P2>, Node<Fr<P3>, Top, Bottom>, Bottom>;
    type S4 = Node<Fr<P1>, N4, N4>;
    assert_type_eq!(<S4 as Clean>::Res, S4);

    type S5 = Node<
        Fr<P1>,
        Node<To<P2>, Bottom, Bottom>,
        Node<
            To<P1>,
            Node<Fr<P2>, Top, Node<Fr<P3>, Top, Bottom>>,
            Node<To<P2>, Node<Fr<P3>, Bottom, Bottom>, Bottom>,
        >,
    >;
    type Clean5 = Node<To<P1>, Node<Fr<P2>, Top, Node<Fr<P3>, Top, Bottom>>, Bottom>;
    assert_type_eq!(<S5 as Clean>::Res, Clean5);

    type S6 =
        Node<Fr<P0>, Top, Node<Fr<P1>, Bottom, Node<Fr<P2>, Node<Fr<P3>, Top, Bottom>, Bottom>>>;
    type Clean6 = Node<Fr<P0>, Top, Node<Fr<P2>, Node<Fr<P3>, Top, Bottom>, Bottom>>;
    assert_type_eq!(<S6 as Clean>::Res, Clean6);
}
