use std::marker::PhantomData;

use crate::assert_type_eq;
use crate::boolean::*;
use crate::kripke_state::{Fr, To};
use crate::nat::*;
use crate::sorted_list::{self, *};
use crate::tools::runtime_sfdd;

mod clean;
mod helper;

mod apply;
mod bool_op;
mod intersect;
mod union;

pub use apply::*;
pub use bool_op::*;
pub use clean::*;
pub use intersect::*;
pub use union::*;

pub use self::helper::IfSfdd;

pub struct Bottom {}
pub struct Top {}

pub trait SfddLabel {
    fn reify() -> runtime_sfdd::El;
}

pub trait Sfdd {
    fn reify() -> runtime_sfdd::Sfdd;
}

pub struct Node<T: SfddLabel, Take: Sfdd, Skip: Sfdd>(
    PhantomData<T>,
    PhantomData<Take>,
    PhantomData<Skip>,
);

impl Sfdd for Bottom {
    fn reify() -> runtime_sfdd::Sfdd {
        runtime_sfdd::Sfdd::Bottom
    }
}

impl Sfdd for Top {
    fn reify() -> runtime_sfdd::Sfdd {
        runtime_sfdd::Sfdd::Top
    }
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Sfdd for Node<T, Take, Skip> {
    fn reify() -> runtime_sfdd::Sfdd {
        runtime_sfdd::Sfdd::Node(T::reify(), Box::new(Take::reify()), Box::new(Skip::reify()))
    }
}

// Utility traits to use in bounds later on
pub trait IsNode: Sfdd {}
impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> IsNode for Node<T, Take, Skip> {}

pub trait IsNotTop: Sfdd {}
impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> IsNotTop for Node<T, Take, Skip> {}
impl IsNotTop for Bottom {}

pub trait IsTerminal: Sfdd {}
impl IsTerminal for Bottom {}
impl IsTerminal for Top {}

// Node-for-node equality of canonical Sfdd
pub trait SfddEq<S: Sfdd>: Sfdd {
    type Res: Bool;
}

impl SfddEq<Bottom> for Bottom {
    type Res = T;
}

impl SfddEq<Top> for Bottom {
    type Res = F;
}

impl SfddEq<Top> for Top {
    type Res = T;
}

impl SfddEq<Bottom> for Top {
    type Res = F;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> SfddEq<Top> for Node<T, Take, Skip> {
    type Res = F;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> SfddEq<Bottom> for Node<T, Take, Skip> {
    type Res = F;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> SfddEq<Node<T, Take, Skip>> for Top {
    type Res = F;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> SfddEq<Node<T, Take, Skip>> for Bottom {
    type Res = F;
}

impl<
        T1: sorted_list::Comparable<T2> + SfddLabel,
        T2: sorted_list::Comparable<T1> + SfddLabel,
        Take1: Sfdd,
        Skip1: Sfdd,
        Take2: Sfdd,
        Skip2: Sfdd,
    > SfddEq<Node<T2, Take2, Skip2>> for Node<T1, Take1, Skip1>
where
    Take1: SfddEq<Take2>,
    Skip1: SfddEq<Skip2>,
{
    type Res = <<<T1 as sorted_list::Comparable<T2>>::Eq as Bool>::And<
        <Take1 as SfddEq<Take2>>::Res,
    > as Bool>::And<<Skip1 as SfddEq<Skip2>>::Res>;
}

pub trait ToSfdd: SortedList {
    type Res: Sfdd;
}

impl ToSfdd for sorted_list::EmptyList {
    type Res = Top;
}

impl<H: sorted_list::Comparable + SfddLabel, R: SortedList> ToSfdd for sorted_list::Cons<H, R>
where
    R: ToSfdd,
{
    type Res = Node<H, <R as ToSfdd>::Res, Bottom>;
}

#[macro_export]
macro_rules! sort_list {
    ($(,)?) => {
        crate::sorted_list::EmptyList
    };
    ($h: path) => {
        crate::sorted_list::Cons<$h, crate::sorted_list::EmptyList>
    };
    ($h:path, $($r:tt)*) => {
        <crate::sort_list!($($r)*) as crate::sorted_list::Add<$h>>::Res
    };
}

#[macro_export]
macro_rules! single_set_sfdd {
    ($($t:tt)*) => { <crate::sort_list!($($t)*) as crate::sfdd::ToSfdd>::Res };
}

#[macro_export]
macro_rules! enc_sfdd {
    ($(,)?) => { crate::sfdd::Bottom };
    (($($h:tt)*)) => {
        crate::single_set_sfdd!($($h)*)
    };
    (($($h:tt)*), $($r:tt)*) => {
        <<crate::single_set_sfdd!($($h)*) as crate::sfdd::Union<crate::enc_sfdd!($($r)*)>>::Res as crate::sfdd::Clean>::Res
    };
}

#[allow(dead_code)]
fn a() {
    type Sfdd1 = Node<P0, Node<P1, Top, Bottom>, Top>;
    type Sfdd2 = Node<P1, Top, Top>;

    type F1 = Fr<P1>;
    type F2 = Fr<P2>;
    type S1 = single_set_sfdd!(F1, F2);
    type S2 = single_set_sfdd!(F2, F1);
    assert_type_eq!(S1, S2);

    type T4 = To<P4>;
    type F8 = Fr<P8>;
    type T9 = To<P9>;
    type S3 = enc_sfdd!((F1, F2), (F2, F1), (T4, T9, F8), ());
    type S4 = enc_sfdd!((F1, F2), ());

    type S3InterS4 = <S3 as Intersect<S4>>::Res;
    assert_type_eq!(S3InterS4, S4);

    type S3InterBottom = <S3 as Intersect<Bottom>>::Res;
    assert_type_eq!(S3InterBottom, Bottom);
}
