use crate::boolean::{F, T};
use crate::sorted_list::Comparable;
use crate::{assert_type_eq, enc_sfdd};

use super::helper::SfddHelp;
use super::{BoolOp, BoolToSfddTerm, Bottom, Clean, Node, Sfdd, SfddLabel, Top};

pub trait Apply<Op: BoolOp, S: Sfdd> {
    type Res: Sfdd;
}

impl<Op: BoolOp> Apply<Op, Bottom> for Bottom
where
    Op::Res<F, F>: BoolToSfddTerm,
{
    type Res = <Op::Res<F, F> as BoolToSfddTerm>::Res;
}

impl<Op: BoolOp> Apply<Op, Top> for Bottom
where
    Op::Res<F, T>: BoolToSfddTerm,
{
    type Res = <Op::Res<F, T> as BoolToSfddTerm>::Res;
}

impl<Op: BoolOp> Apply<Op, Bottom> for Top
where
    Op::Res<T, F>: BoolToSfddTerm,
{
    type Res = <Op::Res<T, F> as BoolToSfddTerm>::Res;
}

impl<Op: BoolOp> Apply<Op, Top> for Top
where
    Op::Res<T, T>: BoolToSfddTerm,
{
    type Res = <Op::Res<T, T> as BoolToSfddTerm>::Res;
}

impl<Op: BoolOp, XF: SfddLabel, FTake: Sfdd, FSkip: Sfdd> Apply<Op, Top> for Node<XF, FTake, FSkip>
where
    FTake: Apply<Op, Top>,
    FSkip: Apply<Op, Top>,
{
    type Res = Node<XF, <FTake as Apply<Op, Top>>::Res, <FSkip as Apply<Op, Top>>::Res>;
}

impl<Op: BoolOp, XF: SfddLabel, FTake: Sfdd, FSkip: Sfdd> Apply<Op, Bottom>
    for Node<XF, FTake, FSkip>
where
    FTake: Apply<Op, Bottom>,
    FSkip: Apply<Op, Bottom>,
{
    type Res = Node<XF, <FTake as Apply<Op, Bottom>>::Res, <FSkip as Apply<Op, Bottom>>::Res>;
}

impl<Op: BoolOp, XG: SfddLabel, GTake: Sfdd, GSkip: Sfdd> Apply<Op, Node<XG, GTake, GSkip>> for Top
where
    Top: Apply<Op, GTake>,
    Top: Apply<Op, GSkip>,
{
    type Res = Node<XG, <Top as Apply<Op, GTake>>::Res, <Top as Apply<Op, GSkip>>::Res>;
}

impl<Op: BoolOp, XG: SfddLabel, GTake: Sfdd, GSkip: Sfdd> Apply<Op, Node<XG, GTake, GSkip>>
    for Bottom
where
    Bottom: Apply<Op, GTake>,
    Bottom: Apply<Op, GSkip>,
{
    type Res = Node<XG, <Bottom as Apply<Op, GTake>>::Res, <Bottom as Apply<Op, GSkip>>::Res>;
}

impl<
        Op: BoolOp,
        XF: SfddLabel + Comparable<XG>,
        FTake: Sfdd,
        FSkip: Sfdd,
        XG: SfddLabel + Comparable<XF>,
        GTake: Sfdd,
        GSkip: Sfdd,
    > Apply<Op, Node<XG, GTake, GSkip>> for Node<XF, FTake, FSkip>
where
    FTake: Apply<Op, Node<XG, GTake, GSkip>>,
    FSkip: Apply<Op, Node<XG, GTake, GSkip>>,
    Node<XF, FTake, FSkip>: Apply<Op, GTake>,
    Node<XF, FTake, FSkip>: Apply<Op, GSkip>,
    FTake: Apply<Op, GTake>,
    FSkip: Apply<Op, GSkip>,
    (
        Node<
            XF,
            <FTake as Apply<Op, Node<XG, GTake, GSkip>>>::Res,
            <FSkip as Apply<Op, Node<XG, GTake, GSkip>>>::Res,
        >,
        Node<XF, <FTake as Apply<Op, GTake>>::Res, <FSkip as Apply<Op, GSkip>>::Res>,
        Node<
            XG,
            <Node<XF, FTake, FSkip> as Apply<Op, GTake>>::Res,
            <Node<XF, FTake, FSkip> as Apply<Op, GSkip>>::Res,
        >,
    ): SfddHelp<XF, XG>,
{
    type Res = <(
        Node<
            XF,
            <FTake as Apply<Op, Node<XG, GTake, GSkip>>>::Res,
            <FSkip as Apply<Op, Node<XG, GTake, GSkip>>>::Res,
        >,
        Node<XF, <FTake as Apply<Op, GTake>>::Res, <FSkip as Apply<Op, GSkip>>::Res>,
        Node<
            XG,
            <Node<XF, FTake, FSkip> as Apply<Op, GTake>>::Res,
            <Node<XF, FTake, FSkip> as Apply<Op, GSkip>>::Res,
        >,
    ) as SfddHelp<XF, XG>>::Res;
}

#[allow(dead_code)]
fn test_apply_and() {
    use super::bool_op::And;
    use crate::kripke_state::*;
    use crate::nat::*;

    type S1 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P3>));
    type S2 = enc_sfdd!((To<P4>), (To<P5>));
    type S1xS2 = <enc_sfdd!(
        (Fr<P1>, Fr<P2>, To<P4>),
        (Fr<P3>, To<P4>),
        (Fr<P1>, Fr<P2>, To<P5>),
        (Fr<P3>, To<P5>)
    ) as Clean>::Res;
    assert_type_eq!(S1xS2, <<S1 as Apply<And, S2>>::Res as Clean>::Res);
    assert_type_eq!(S1xS2, <<S2 as Apply<And, S1>>::Res as Clean>::Res);

    type S3 = enc_sfdd!((Fr<P1>));
    type S4 = enc_sfdd!(());
    type S3xS4 = enc_sfdd!((Fr<P1>));
    assert_type_eq!(S3xS4, <<S3 as Apply<And, S4>>::Res as Clean>::Res);
    assert_type_eq!(S3xS4, <<S4 as Apply<And, S3>>::Res as Clean>::Res);

    type S5 = enc_sfdd!((Fr<P1>));
    type S6 = enc_sfdd!((Fr<P2>));
    type S5xS6 = enc_sfdd!((Fr<P1>, Fr<P2>));
    assert_type_eq!(S5xS6, <<S5 as Apply<And, S6>>::Res as Clean>::Res);
    assert_type_eq!(S5xS6, <<S6 as Apply<And, S5>>::Res as Clean>::Res);

    type S7 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>), (Fr<P4>));
    type S8 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    type S7xS8 = enc_sfdd!(
        (Fr<P1>, Fr<P3>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P2>, Fr<P3>),
        (Fr<P1>, Fr<P2>, Fr<P3>),
        (Fr<P1>, Fr<P2>, Fr<P4>),
        (Fr<P1>, Fr<P3>, Fr<P4>)
    );
    // assert_type_eq!(S7xS8, <<S7 as Apply<And, S8>>::Res as Clean>::Res);
    // assert_type_eq!(S7xS8, <<S8 as Apply<And, S7>>::Res as Clean>::Res);
}
