use crate::boolean::{Bool, F, T};

use super::{Comparable, Sfdd};

pub trait IfSfdd<C: Bool> {
    type Res: Sfdd;
}

impl<A: Sfdd, B: Sfdd> IfSfdd<T> for (A, B) {
    type Res = A;
}

impl<A: Sfdd, B: Sfdd> IfSfdd<F> for (A, B) {
    type Res = B;
}

// Helper for case separation in union/intersection definition
// Selects Sfdd from 3-tuple based on T1 <|=|> T2
pub trait SfddHelp<T1: Comparable<T2>, T2: Comparable<T1>> {
    type Res: Sfdd;
}

impl<T1: Comparable<T2>, T2: Comparable<T1>, S1: Sfdd, S2: Sfdd, S3: Sfdd> SfddHelp<T1, T2>
    for (S1, S2, S3)
where
    (S3, S2): IfSfdd<<T1 as Comparable<T2>>::Gt>,
    (S1, <(S3, S2) as IfSfdd<<T1 as Comparable<T2>>::Gt>>::Res): IfSfdd<<T1 as Comparable<T2>>::Lt>,
{
    // S1 if T1 < T2, S2 if T1 = T2, S3 otherwise
    type Res = <(S1, <(S3, S2) as IfSfdd<<T1 as Comparable<T2>>::Gt>>::Res) as IfSfdd<
        <T1 as Comparable<T2>>::Lt,
    >>::Res;
}
