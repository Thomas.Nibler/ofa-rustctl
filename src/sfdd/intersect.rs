use super::{helper::SfddHelp, Bottom, Comparable, Node, Sfdd, SfddLabel, Top};

pub trait Intersect<S: Sfdd> {
    type Res: Sfdd;
}

// Base cases
impl<S: Sfdd> Intersect<Bottom> for S {
    type Res = Bottom;
}

impl Intersect<Top> for Top {
    type Res = Top;
}

impl Intersect<Top> for Bottom {
    type Res = Bottom;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Intersect<Node<T, Take, Skip>> for Top
where
    Skip: Intersect<Top>,
{
    type Res = <Skip as Intersect<Top>>::Res;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Intersect<Top> for Node<T, Take, Skip>
where
    Skip: Intersect<Top>,
{
    type Res = <Skip as Intersect<Top>>::Res;
}

impl<T: SfddLabel, Take: Sfdd, Skip: Sfdd> Intersect<Node<T, Take, Skip>> for Bottom {
    type Res = Bottom;
}

// Recursive case
impl<
        T1: Comparable<T2> + SfddLabel,
        Take1: Sfdd,
        Skip1: Sfdd,
        T2: Comparable<T1> + SfddLabel,
        Take2: Sfdd,
        Skip2: Sfdd,
    > Intersect<Node<T2, Take2, Skip2>> for Node<T1, Take1, Skip1>
where
    Skip1: Intersect<Node<T2, Take2, Skip2>>,
    Node<T1, Take1, Skip1>: Intersect<Skip2>,
    Take1: Intersect<Take2>,
    Skip1: Intersect<Skip2>,
    (
        <Skip1 as Intersect<Node<T2, Take2, Skip2>>>::Res,
        Node<T1, <Take1 as Intersect<Take2>>::Res, <Skip1 as Intersect<Skip2>>::Res>,
        <Node<T1, Take1, Skip1> as Intersect<Skip2>>::Res,
    ): SfddHelp<T1, T2>,
{
    type Res = <(
        <Skip1 as Intersect<Node<T2, Take2, Skip2>>>::Res,
        Node<T1, <Take1 as Intersect<Take2>>::Res, <Skip1 as Intersect<Skip2>>::Res>,
        <Node<T1, Take1, Skip1> as Intersect<Skip2>>::Res,
    ) as SfddHelp<T1, T2>>::Res;
}
