#[macro_export]
macro_rules! ctl {
    ($k:path, AF $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Af<$k, ctl_arg!($k, $r)>>::Res
    };
    ($k:path, AG $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Ag<$k, ctl_arg!($k, $r)>>::Res
    };
    ($k:path, $l:tt AU $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Au<$k, ctl_arg!($k, $l), ctl_arg!($k, $r)>>::Res
    };
    ($k:path, EF $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Ef<$k, ctl_arg!($k, $r)>>::Res
    };
    ($k:path, EG $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Eg<$k, ctl_arg!($k, $r)>>::Res
    };
    ($k:path, $l:tt EU $r:tt) => {
        <crate::ctl::Ctl as crate::ctl::Eu<$k, ctl_arg!($k, $l), ctl_arg!($k, $r)>>::Res
    };
    ($k:path, $($r:tt)*) => {
        $($r)*
    };
}

macro_rules! ctl_arg {
    ($k:path, ( $r:tt )) => {
        ctl!($k, $r)
    };
    ($k:path, ( $($r:tt)* )) => {
           ctl!($k, $($r)*)
    };
}

macro_rules! or {
    ($a:path, $b:path) => {
        <$a as crate::sfdd::Union<$b>>::Res
    };
}

macro_rules! and {
    ($a:path, $b:path) => {
        <$a as crate::sfdd::Intersect<$b>>::Res
    };
}

macro_rules! not {
    ($s:path, $k:path) => {
        <<<$k as crate::sfdd_state_utils::DropToNodes>::Res as crate::sfdd::Apply<
            crate::sfdd::AndNot,
            $s,
        >>::Res as crate::sfdd::Clean>::Res
    };
}
