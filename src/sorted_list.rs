use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::*,
    kripke_state::{self, *},
    nat::*,
};

pub trait Comparable<T = Self> {
    type Eq: Bool;
    type Lt: Bool;
    type Gt: Bool;
}

impl<N: kripke_state::Compare<M>, M: kripke_state::Compare<N>> Comparable<M> for N {
    type Eq = <N as Compare<M>>::Eq;
    type Lt = <N as Compare<M>>::Lt;
    type Gt = <N as Compare<M>>::Gt;
}

pub trait SortedList {}

pub struct EmptyList {
    _private: (),
}

pub struct Cons<H, R: SortedList> {
    _marker_h: PhantomData<H>,
    _marker_r: PhantomData<R>,
}

impl SortedList for EmptyList {}

impl<H: Comparable, R: SortedList> SortedList for Cons<H, R> {}

pub trait Add<N> {
    type Res: SortedList;
}

impl<N: Comparable> Add<N> for EmptyList {
    type Res = Cons<N, EmptyList>;
}

impl<N: Comparable<H>, H: Comparable<N>, R: SortedList> Add<N> for Cons<H, R>
where
    (N, H): IfCmpable<<N as Comparable<H>>::Lt>,
    (Cons<H, R>, <R as Add<N>>::Res): IfSeq<<N as Comparable<H>>::Lt>,
    R: Add<N>,
{
    type Res = Cons<
        <(N, H) as IfCmpable<<N as Comparable<H>>::Lt>>::Res,
        <(Cons<H, R>, <R as Add<N>>::Res) as IfSeq<<N as Comparable<H>>::Lt>>::Res,
    >;
}

pub trait IfCmpable<C: Bool> {
    type Res: Comparable;
}

impl<A: Comparable, B: Comparable> IfCmpable<T> for (A, B) {
    type Res = A;
}

impl<A: Comparable, B: Comparable> IfCmpable<F> for (A, B) {
    type Res = B;
}

pub trait IfSeq<C: Bool> {
    type Res: SortedList;
}

impl<A: SortedList, B: SortedList> IfSeq<T> for (A, B) {
    type Res = A;
}

impl<A: SortedList, B: SortedList> IfSeq<F> for (A, B) {
    type Res = B;
}

#[allow(dead_code)]
fn a() {
    type F2 = Fr<P2>;
    type F3 = Fr<P3>;
    type T3 = To<P3>;
    type T4 = To<P4>;

    type L3 = sort_list!(T4, F3, F2, T3);
    type L4 = Cons<F2, Cons<F3, Cons<T3, Cons<T4, EmptyList>>>>;
    assert_type_eq!(L3, L4);
    assert_type_eq!(EmptyList, sort_list!());
    assert_type_eq!(Cons<Fr<P2>, EmptyList>, sort_list!(Fr<P2>));
}
