/// makes a sorted list of numbers, wrapping each in kripke_state::Fr<_>
#[macro_export]
macro_rules! __fr_sort_list {
    ($(,)?) => {
        crate::sorted_list::EmptyList
    };
    ($h: path) => {
        crate::sorted_list::Cons<crate::kripke_state::Fr<$h>, crate::sorted_list::EmptyList>
    };
    ($h:path, $($r:tt)*) => {
        <crate::__fr_sort_list!($($r)*) as crate::sorted_list::Add<crate::kripke_state::Fr<$h>>>::Res
    };
}

/// makes sfdd from a list of numbers, wrapping each in kripke_state::Fr<_>
#[macro_export]
macro_rules! __sfdd_fr {
    ($(,)?) => {
        crate::sfdd::Bottom
    };
    ($($t:tt)*) => {
        <crate::__fr_sort_list!($($t)*) as crate::sfdd::ToSfdd>::Res
    };
}

/// makes a sorted list of numbers, wrapping each in kripke_state::To<_>
#[macro_export]
macro_rules! __to_sort_list {
    ($(,)?) => {
        crate::sorted_list::EmptyList
    };
    ($h: path) => {
        crate::sorted_list::Cons<crate::kripke_state::To<$h>, crate::sorted_list::EmptyList>
    };
    ($h:path, $($r:tt)*) => {
        <crate::__to_sort_list!($($r)*) as crate::sorted_list::Add<crate::kripke_state::To<$h>>>::Res
    };
}

/// makes sfdd from a list of numbers, wrapping each in kripke_state::Fr<_>
#[macro_export]
macro_rules! __sfdd_to {
    ($(,)?) => {
        crate::sfdd::Bottom
    };
    ($($t:tt)*) => {
        <crate::__to_sort_list!($($t)*) as crate::sfdd::ToSfdd>::Res
    };
}

/// Very fancy way to encode Kripke structures as follows:
/// ```
/// kripke!(
///    (P1, P2) -> (P3),
///    (P1, P2) -> (P4),
///    (P3) -> (P1, P2),
///    (P4) -> (P3)
/// )
/// ````
#[macro_export]
macro_rules! kripke {
    ($(,)?) => {
        crate::sfdd::Bottom
    };
    (($($fr:tt)*) -> ($($to:tt)*)) => {
        <<crate::__sfdd_fr!($($fr)*) as crate::sfdd::Apply<crate::sfdd::And, crate::__sfdd_to!($($to)*)>>::Res as crate::sfdd::Clean>::Res
    };
    (($($fr:tt)*) -> ($($to:tt)*), $($rest:tt)*) => {
        <<<crate::__sfdd_fr!($($fr)*) as crate::sfdd::Apply<crate::sfdd::And, crate::__sfdd_to!($($to)*)>>::Res as crate::sfdd::Union<crate::kripke!($($rest)*)>>::Res as crate::sfdd::Clean>::Res
    };
}

#[macro_export]
macro_rules! single_state_set {
    (@sort_list $(,)?) => {
        crate::sorted_list::EmptyList
    };
    (@sort_list $h: path) => {
        crate::sorted_list::Cons<crate::kripke_state::Fr<$h>, crate::sorted_list::EmptyList>
    };
    (@sort_list $h:path, $($r:tt)*) => {
        <crate::single_state_set!(@sort_list $($r)*) as crate::sorted_list::Add<crate::kripke_state::Fr<$h>>>::Res
    };
    ($($t:tt)*) => { <crate::single_state_set!(@sort_list $($t)*) as crate::sfdd::ToSfdd>::Res };
}

#[macro_export]
macro_rules! state_set {
    ($(,)?) => { crate::sfdd::Bottom };
    (($($h:tt)*)) => {
        crate::single_state_set!($($h)*)
    };
    (($($h:tt)*), $($r:tt)*) => {
        <<crate::single_state_set!($($h)*) as crate::sfdd::Union<crate::state_set!($($r)*)>>::Res as crate::sfdd::Clean>::Res
    };
}

#[allow(dead_code)]
fn test() {
    use crate::assert_type_eq;
    use crate::kripke_state::*;
    use crate::nat::*;
    use crate::sfdd::*;

    assert_type_eq!(
        sort_list!(Fr<P1>, Fr<P2>, Fr<P3>, Fr<P4>),
        __fr_sort_list!(P1, P2, P3, P4)
    );
    type K = kripke!(
        (P1, P2) -> (P3, P4),
        (P2) -> (P5),
        (P2) -> (P3, P4)
    );
    type ExpectedK = <single_set_sfdd!(Fr<P1>, Fr<P2>, To<P3>, To<P4>) as Union<
        <single_set_sfdd!(Fr<P2>, To<P5>) as Union<single_set_sfdd!(Fr<P2>, To<P3>, To<P4>)>>::Res,
    >>::Res;
    assert_type_eq!(K, <ExpectedK as Clean>::Res);

    type K1 = kripke!(
        (P1, P2) -> (P3, P4),
    );
    type ExpectedK1 = single_set_sfdd!(Fr<P1>, Fr<P2>, To<P3>, To<P4>);
    assert_type_eq!(K1, ExpectedK1);
}
