use std::collections::{BTreeSet, HashMap, HashSet};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Sfdd {
    Top,
    Bottom,
    Node(El, Box<Sfdd>, Box<Sfdd>),
}

#[derive(Debug, Clone, PartialEq, Eq, Ord, Hash, Copy)]
pub enum El {
    Fr(i32),
    To(i32),
}

impl std::fmt::Display for El {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            El::Fr(x) => write!(f, "{}", x),
            El::To(x) => write!(f, "\"{}'\"", x),
        }
    }
}

impl std::cmp::PartialOrd for El {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match (self, other) {
            (El::To(a), El::Fr(b)) => {
                if a == b {
                    Some(std::cmp::Ordering::Greater)
                } else {
                    a.partial_cmp(b)
                }
            }
            (El::Fr(a), El::To(b)) => {
                if a == b {
                    Some(std::cmp::Ordering::Less)
                } else {
                    a.partial_cmp(b)
                }
            }
            (El::Fr(a), El::Fr(b)) => a.partial_cmp(b),
            (El::To(a), El::To(b)) => a.partial_cmp(b),
        }
    }
}

impl Sfdd {
    pub fn decode(&self) -> BTreeSet<BTreeSet<El>> {
        match self {
            Sfdd::Top => [BTreeSet::default()].into_iter().collect(),
            Sfdd::Bottom => BTreeSet::default(),
            Sfdd::Node(n, take, skip) => {
                let mut dec = skip.decode();
                dec.extend(
                    take.decode()
                        .into_iter()
                        .map(|f| std::iter::once(*n).chain(f).collect()),
                );
                dec
            }
        }
    }

    pub fn clean(&self) -> Sfdd {
        match self {
            Sfdd::Top => Sfdd::Top,
            Sfdd::Bottom => Sfdd::Bottom,
            Sfdd::Node(_, take, skip) if take == skip => take.clean(),
            Sfdd::Node(t, take, skip) => match take.as_ref() {
                Sfdd::Bottom => skip.clean(),
                _ => {
                    let tc = take.clean();
                    match tc {
                        Sfdd::Bottom => skip.clean(),
                        tc => {
                            let sc = skip.clean();
                            if sc == tc {
                                sc
                            } else {
                                Sfdd::Node(*t, Box::new(tc), Box::new(sc))
                            }
                        }
                    }
                }
            },
        }
    }

    pub fn union(&self, other: &Sfdd) -> Sfdd {
        match (self, other) {
            (s, Sfdd::Bottom) => s.clone(),
            (Sfdd::Bottom, s) => s.clone(),
            (Sfdd::Top, Sfdd::Top) => Sfdd::Top,
            (Sfdd::Top, Sfdd::Node(t, take, skip)) => {
                Sfdd::Node(*t, take.clone(), Box::new(skip.union(&Sfdd::Top)))
            }
            (Sfdd::Node(t, take, skip), Sfdd::Top) => {
                Sfdd::Node(*t, take.clone(), Box::new(skip.union(&Sfdd::Top)))
            }
            (Sfdd::Node(t1, take1, skip1), Sfdd::Node(t2, take2, skip2)) => match t1.cmp(t2) {
                std::cmp::Ordering::Less => {
                    Sfdd::Node(*t1, take1.clone(), Box::new(skip1.union(other)))
                }
                std::cmp::Ordering::Equal => Sfdd::Node(
                    *t1,
                    Box::new(take1.union(take2)),
                    Box::new(skip1.union(skip2)),
                ),
                std::cmp::Ordering::Greater => {
                    Sfdd::Node(*t2, take2.clone(), Box::new(self.union(skip2)))
                }
            },
        }
    }

    pub fn intersect(&self, other: &Sfdd) -> Sfdd {
        match (self, other) {
            (&Sfdd::Top, &Sfdd::Top) => Sfdd::Top,
            (&Sfdd::Bottom, _) | (_, &Sfdd::Bottom) => Sfdd::Bottom,
            (Sfdd::Top, Sfdd::Node(_, _, skip)) => skip.intersect(&Sfdd::Top),
            (Sfdd::Node(_, _, skip), Sfdd::Top) => skip.intersect(&Sfdd::Top),
            (s1 @ Sfdd::Node(t1, take1, skip1), s2 @ Sfdd::Node(t2, take2, skip2)) => {
                match t1.cmp(t2) {
                    std::cmp::Ordering::Less => skip1.intersect(s2),
                    std::cmp::Ordering::Equal => Sfdd::Node(
                        *t1,
                        Box::new(take1.intersect(take2)),
                        Box::new(skip1.intersect(skip2)),
                    ),
                    std::cmp::Ordering::Greater => s1.intersect(skip2),
                }
            }
        }
    }
}

pub fn powerset(els: impl IntoIterator<Item = El>) -> Sfdd {
    fn ps(mut els: impl Iterator<Item = El>) -> Sfdd {
        match els.next() {
            Some(x) => {
                let t = Box::new(ps(els));
                Sfdd::Node(x, t.clone(), t)
            }
            None => Sfdd::Top,
        }
    }

    let mut els: Vec<_> = els.into_iter().collect();
    els.sort();
    ps(els.into_iter())
}

pub trait BoolOp {
    fn op(a: bool, b: bool) -> bool;
}

pub struct Or {}

impl BoolOp for Or {
    fn op(a: bool, b: bool) -> bool {
        a || b
    }
}

pub struct And {}

impl BoolOp for And {
    fn op(a: bool, b: bool) -> bool {
        a && b
    }
}

pub struct AndNot {}

impl BoolOp for AndNot {
    fn op(a: bool, b: bool) -> bool {
        a && !b
    }
}

pub struct Implies {}

impl BoolOp for Implies {
    fn op(a: bool, b: bool) -> bool {
        !a || b
    }
}

pub fn apply<Op: BoolOp>(f: &Sfdd, g: &Sfdd) -> Sfdd {
    match (f, g) {
        (Sfdd::Node(xf, tf, sf), Sfdd::Node(xg, tg, sg)) if xf == xg => Sfdd::Node(
            *xf,
            Box::new(apply::<Op>(tf, tg)),
            Box::new(apply::<Op>(sf, sg)),
        ),
        (Sfdd::Node(xf, tf, sf), g @ (Sfdd::Top | Sfdd::Bottom)) => Sfdd::Node(
            *xf,
            Box::new(apply::<Op>(tf, g)),
            Box::new(apply::<Op>(sf, g)),
        ),
        (f @ (Sfdd::Top | Sfdd::Bottom), Sfdd::Node(xg, tg, sg)) => Sfdd::Node(
            *xg,
            Box::new(apply::<Op>(f, tg)),
            Box::new(apply::<Op>(f, sg)),
        ),
        (f @ (Sfdd::Top | Sfdd::Bottom), g @ (Sfdd::Top | Sfdd::Bottom)) => {
            let bf = match f {
                Sfdd::Top => true,
                Sfdd::Bottom => false,
                Sfdd::Node(_, _, _) => unreachable!(),
            };
            let bg = match g {
                Sfdd::Top => true,
                Sfdd::Bottom => false,
                Sfdd::Node(_, _, _) => unreachable!(),
            };
            match Op::op(bf, bg) {
                true => Sfdd::Top,
                false => Sfdd::Bottom,
            }
        }
        (nf @ Sfdd::Node(xf, tf, sf), ng @ Sfdd::Node(xg, tg, sg)) => {
            if xf < xg {
                Sfdd::Node(
                    *xf,
                    Box::new(apply::<Op>(tf, ng)),
                    Box::new(apply::<Op>(sf, ng)),
                )
            } else if xg < xf {
                Sfdd::Node(
                    *xg,
                    Box::new(apply::<Op>(nf, tg)),
                    Box::new(apply::<Op>(nf, sg)),
                )
            } else {
                unreachable!()
            }
        }
    }
}

#[test]
fn apply_and() {
    let s1: Sfdd = set_to_sfdd([El::Fr(1), El::Fr(2), El::Fr(3)]).union(&set_to_sfdd([El::Fr(1)]));
    let s2: Sfdd = set_to_sfdd([El::Fr(4), El::Fr(6)]).union(&set_to_sfdd([El::Fr(7)]));
    let expected_s1s2: BTreeSet<BTreeSet<_>> = [
        [El::Fr(1), El::Fr(2), El::Fr(3), El::Fr(4), El::Fr(6)]
            .into_iter()
            .collect(),
        [El::Fr(1), El::Fr(4), El::Fr(6)].into_iter().collect(),
        [El::Fr(1), El::Fr(2), El::Fr(3), El::Fr(7)]
            .into_iter()
            .collect(),
        [El::Fr(1), El::Fr(7)].into_iter().collect(),
    ]
    .into_iter()
    .collect();
    let actual = apply::<And>(&s1, &s2);
    assert_eq!(expected_s1s2, actual.decode());
}

pub fn set_to_sfdd(els: impl IntoIterator<Item = El>) -> Sfdd {
    let unique: BTreeSet<_> = els.into_iter().collect();
    let mut sorted: Vec<_> = unique.into_iter().collect();
    sorted.sort();
    sorted_to_sfdd(sorted.into_iter())
}

fn sorted_to_sfdd(mut els: impl Iterator<Item = El>) -> Sfdd {
    match els.next() {
        None => Sfdd::Top,
        Some(n) => Sfdd::Node(n, Box::new(sorted_to_sfdd(els)), Box::new(Sfdd::Bottom)),
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum EdgeType {
    Take,
    Skip,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum Node {
    Top,
    Bottom,
    Node { id: i32, label: El },
}

impl Sfdd {
    fn node(&self, ids: &HashMap<Sfdd, i32>) -> Node {
        match self {
            Sfdd::Bottom => Node::Bottom,
            Sfdd::Top => Node::Top,
            Sfdd::Node(t, _, _) => Node::Node {
                id: *ids
                    .get(self)
                    .expect("node must be inserted in assign_node_ids"),
                label: t.clone(),
            },
        }
    }

    fn collect_edges(&self, node_ids: &HashMap<Sfdd, i32>) -> Vec<(Node, Node, EdgeType)> {
        match self {
            Sfdd::Bottom | Sfdd::Top => Vec::default(),
            Sfdd::Node(t, take, skip) => {
                let this_id = *node_ids
                    .get(self)
                    .expect("node must be inserted in assign_node_ids");
                let mut edges = vec![
                    (
                        Node::Node {
                            id: this_id,
                            label: t.clone(),
                        },
                        take.node(node_ids),
                        EdgeType::Take,
                    ),
                    (
                        Node::Node {
                            id: this_id,
                            label: t.clone(),
                        },
                        skip.node(node_ids),
                        EdgeType::Skip,
                    ),
                ];
                edges.extend(take.collect_edges(node_ids));
                edges.extend(skip.collect_edges(node_ids));
                edges
            }
        }
    }
}

fn assign_node_ids(sfdd: &Sfdd, cur_id: &mut i32, nodes: &mut HashMap<Sfdd, i32>) {
    match sfdd {
        Sfdd::Node(_, take, skip) => {
            if !nodes.contains_key(sfdd) {
                nodes.insert(sfdd.clone(), *cur_id);
                *cur_id += 1;
            }
            assign_node_ids(take, cur_id, nodes);
            assign_node_ids(skip, cur_id, nodes);
        }
        Sfdd::Top | Sfdd::Bottom => {}
    }
}

fn node_decl_str(n: &Node) -> String {
    match n {
        Node::Top => "".into(),
        Node::Bottom => "".into(),
        Node::Node { id, label } => format!("\t{} [label={}]\n", id, label),
    }
}

fn node_str(n: &Node) -> String {
    match n {
        Node::Top => "top".into(),
        Node::Bottom => "bottom".into(),
        Node::Node { id, label: _ } => format!("{}", id),
    }
}

pub fn graphviz(sfdd: &Sfdd) -> String {
    let mut id = 0;
    let mut node_ids: HashMap<Sfdd, i32> = HashMap::default();

    assign_node_ids(sfdd, &mut id, &mut node_ids);

    let edges: HashSet<_> = sfdd.collect_edges(&node_ids).into_iter().collect();
    let node_list: String = node_ids
        .iter()
        .map(|(sfdd, _id)| node_decl_str(&sfdd.node(&node_ids)))
        .collect();
    let edge_list: String = edges
        .into_iter()
        .map(|(f, t, ty)| {
            let edge_style = match ty {
                EdgeType::Take => "",
                EdgeType::Skip => "[style=dashed]",
            };
            let f_str = node_str(&f);
            let t_str = node_str(&t);
            format!("\t{} -> {}{}\n", f_str, t_str, edge_style)
        })
        .collect();
    format!(
        r#"
digraph F {{
    top [ label =<&#8868;> ]
    bottom [ label =<&#8869;> ]
{}
{}
    }}
    "#,
        node_list, edge_list
    )
}

#[test]
fn single_set() {
    let s1: Sfdd = set_to_sfdd([El::Fr(1), El::Fr(2), El::Fr(3)]);
    assert_eq!(
        s1,
        Sfdd::Node(
            El::Fr(1),
            Box::new(Sfdd::Node(
                El::Fr(2),
                Box::new(Sfdd::Node(
                    El::Fr(3),
                    Box::new(Sfdd::Top),
                    Box::new(Sfdd::Bottom)
                )),
                Box::new(Sfdd::Bottom)
            )),
            Box::new(Sfdd::Bottom)
        )
    )
}

#[test]
fn single_set_round_trip() {
    let s1 = [El::Fr(1), El::Fr(3), El::Fr(5), El::Fr(0)];
    assert_eq!(
        [s1.iter().copied().collect::<BTreeSet<El>>()]
            .into_iter()
            .collect::<BTreeSet<BTreeSet<_>>>(),
        set_to_sfdd(s1).decode()
    );
    let s2 = [];
    assert_eq!(
        [BTreeSet::<El>::new()].into_iter().collect::<BTreeSet<_>>(),
        set_to_sfdd(s2).decode()
    );
}

#[allow(dead_code)]
fn sf(l: impl IntoIterator<Item = impl IntoIterator<Item = El>>) -> BTreeSet<BTreeSet<El>> {
    l.into_iter().map(|s| s.into_iter().collect()).collect()
}

#[test]
fn union() {
    let s1: Sfdd = set_to_sfdd([El::Fr(1), El::Fr(2), El::Fr(3)]);
    let s2: Sfdd = set_to_sfdd([El::Fr(4), El::Fr(6)]);
    let s3: Sfdd = set_to_sfdd([]);

    let s1s3 = sf(vec![vec![], vec![El::Fr(1), El::Fr(2), El::Fr(3)]]);
    assert_eq!(s1s3, s1.union(&s3).decode());
    assert_eq!(s1s3, s3.union(&s1).decode());

    let s1s2 = sf(vec![
        vec![El::Fr(1), El::Fr(2), El::Fr(3)],
        vec![El::Fr(4), El::Fr(6)],
    ]);
    assert_eq!(s1s2, s1.union(&s2).decode());
    assert_eq!(s1s2, s2.union(&s1).decode(), "{:?}", s2.union(&s1));
}

#[test]
fn test_clean() {
    let s1 = node(El::Fr(1), Sfdd::Bottom, Sfdd::Bottom);
    assert_eq!(Sfdd::Bottom, s1.clean());
    let s2 = node(El::Fr(1), Sfdd::Bottom, Sfdd::Top);
    assert_eq!(Sfdd::Top, s2.clean());

    let s3 = node(
        El::Fr(1),
        node(El::Fr(2), Sfdd::Bottom, Sfdd::Top),
        Sfdd::Top,
    );
    assert_eq!(s3.clean(), Sfdd::Top);

    let n4 = node(
        El::Fr(2),
        node(El::Fr(3), Sfdd::Top, Sfdd::Bottom),
        Sfdd::Bottom,
    );
    let s4 = node(El::Fr(1), n4.clone(), n4.clone());
    assert_eq!(s4.clean(), n4);

    let s5 = node(
        El::Fr(1),
        node(El::To(2), Sfdd::Bottom, Sfdd::Bottom),
        node(
            El::To(1),
            node(
                El::Fr(2),
                Sfdd::Top,
                node(El::Fr(3), Sfdd::Top, Sfdd::Bottom),
            ),
            node(
                El::To(2),
                node(El::Fr(3), Sfdd::Bottom, Sfdd::Bottom),
                Sfdd::Bottom,
            ),
        ),
    );
    let clean5 = node(
        El::To(1),
        node(
            El::Fr(2),
            Sfdd::Top,
            node(El::Fr(3), Sfdd::Top, Sfdd::Bottom),
        ),
        Sfdd::Bottom,
    );
    assert_eq!(clean5, s5.clean());
}
