use crate::{
    kripke_state,
    nat::Nat,
    sfdd::{self, SfddLabel},
};

pub trait Reify {
    const REIFY: ToOrFrom<i32>;
}

pub trait DecodeSfdd {
    fn decode() -> Vec<Vec<ToOrFrom<i32>>>;
}

impl DecodeSfdd for sfdd::Bottom {
    fn decode() -> Vec<Vec<ToOrFrom<i32>>> {
        // empty set family
        Vec::default()
    }
}

impl DecodeSfdd for sfdd::Top {
    fn decode() -> Vec<Vec<ToOrFrom<i32>>> {
        // family containing empty set
        vec![Vec::default()]
    }
}

impl<T: Reify + SfddLabel, Take: sfdd::Sfdd + DecodeSfdd, Skip: sfdd::Sfdd + DecodeSfdd> DecodeSfdd
    for sfdd::Node<T, Take, Skip>
{
    fn decode() -> Vec<Vec<ToOrFrom<i32>>> {
        let mut dec = Skip::decode();
        dec.extend(
            Take::decode()
                .into_iter()
                .map(|f| std::iter::once(T::REIFY).chain(f).collect()),
        );
        dec
    }
}

#[derive(Debug)]
pub enum ToOrFrom<T> {
    From(T),
    To(T),
}

impl<N: Nat> Reify for kripke_state::Fr<N> {
    const REIFY: ToOrFrom<i32> = ToOrFrom::From(N::REIFY);
}

impl<N: Nat> Reify for kripke_state::To<N> {
    const REIFY: ToOrFrom<i32> = ToOrFrom::To(N::REIFY);
}

pub fn decode_kripke_transitions<K: sfdd::Sfdd + DecodeSfdd>() -> Vec<(i32, i32)> {
    K::decode()
        .into_iter()
        .map(|v| {
            assert!(v.len() == 2);
            match (&v[0], &v[1]) {
                (ToOrFrom::From(f), ToOrFrom::To(t)) => (*f, *t),
                (ToOrFrom::To(t), ToOrFrom::From(f)) => (*f, *t),
                _ => panic!("only exactly one From and To should exist per set in family"),
            }
        })
        .collect()
}
