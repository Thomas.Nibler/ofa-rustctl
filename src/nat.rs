use std::marker::PhantomData;

use crate::boolean::*;

pub struct Zero;
pub struct Suc<N: Nat>(PhantomData<N>);

pub trait Nat {
    type IsZero: Bool;
    type Suc: Nat;
    type Pred: Nat;
    type Add<A: Nat>: Nat;

    type Lt<A: Nat>: Bool;
    type Eq<A: Nat>: Bool;
    type Gt<A: Nat>: Bool;

    const REIFY: i32;
}

impl Nat for Zero {
    type IsZero = T;
    type Suc = Suc<Zero>;
    type Pred = Zero;
    type Add<A: Nat> = A;

    type Lt<A: Nat> = <A::IsZero as Bool>::Not;
    type Eq<A: Nat> = A::IsZero;
    type Gt<A: Nat> = F;

    const REIFY: i32 = 0;
}

impl<N: Nat> Nat for Suc<N> {
    type IsZero = F;
    type Suc = Suc<Suc<N>>;
    type Pred = N;
    type Add<A: Nat> = Suc<N::Add<A>>;

    type Lt<A: Nat> = <<Self::IsZero as Bool>::And<<A::IsZero as Bool>::Not> as Bool>::Or<
        <Self::Pred as Nat>::Lt<A::Pred>,
    >;
    type Eq<A: Nat> =
        <<Self::IsZero as Bool>::Eq<A::IsZero> as Bool>::And<<Self::Pred as Nat>::Eq<A::Pred>>;
    type Gt<A: Nat> = <<<Self::IsZero as Bool>::Not as Bool>::And<A::IsZero> as Bool>::Or<
        <Self::Pred as Nat>::Gt<A::Pred>,
    >;

    const REIFY: i32 = <N as Nat>::REIFY + 1;
}

pub type P0 = Zero;
pub type P1 = Suc<P0>;
pub type P2 = Suc<P1>;
pub type P3 = Suc<P2>;
pub type P4 = Suc<P3>;
pub type P5 = Suc<P4>;
pub type P6 = Suc<P5>;
pub type P7 = Suc<P6>;
pub type P8 = Suc<P7>;
pub type P9 = Suc<P8>;
pub type P10 = Suc<P9>;
pub type P11 = Suc<P10>;
pub type P12 = Suc<P11>;

#[macro_export]
macro_rules! assert_type_eq {
    ($a:ty, $b:ty) => {
        const _: std::marker::PhantomData<$a> = std::marker::PhantomData::<$b>;
    };
}

#[allow(dead_code)]
fn a() {
    assert_type_eq!(T, <P0 as Nat>::Eq<P0>);
    assert_type_eq!(F, <P0 as Nat>::Lt<P0>);
    assert_type_eq!(F, <P0 as Nat>::Gt<P0>);

    assert_type_eq!(T, <P2 as Nat>::Eq<P2>);
    assert_type_eq!(F, <P2 as Nat>::Lt<P2>);
    assert_type_eq!(F, <P2 as Nat>::Gt<P2>);

    assert_type_eq!(T, <P0 as Nat>::Lt<P1>);
    assert_type_eq!(T, <P2 as Nat>::Lt<P5>);

    assert_type_eq!(F, <P0 as Nat>::Gt<P1>);
    assert_type_eq!(T, <P1 as Nat>::Gt<P0>);
    assert_type_eq!(F, <P2 as Nat>::Gt<P5>);
    assert_type_eq!(T, <P5 as Nat>::Gt<P2>);
    assert_type_eq!(F, <P0 as Nat>::Gt<P0>);

    assert_type_eq!(P9, <P4 as Nat>::Add<<P1 as Nat>::Add<P4>>);
    assert_type_eq!(P11, <P4 as Nat>::Add<<P1 as Nat>::Add<P6>>);
}
