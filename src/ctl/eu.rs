use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::{Bool, F, T},
    kripke,
    kripke_state::Fr,
    predecessor::{PreExistTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq, Union},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes, OnlyToNodes, ToToFrom},
};

use super::Ctl;

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> Eu<K, S, U> for Ctl
where
    _EuHelp: _EuHelpTrait<K, S, U>,
    EuImpl<K, S, U, crate::sfdd::Bottom, F>: EuImplTrait,
{
    type Res = <EuImpl<K, S, U, crate::sfdd::Bottom, F> as EuImplTrait>::Res;
}

pub trait Eu<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

pub struct EuImpl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<U>,
    PhantomData<Y>,
    PhantomData<Stop>,
);

pub trait EuImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes> EuImplTrait
    for EuImpl<K, S, U, Y, T>
where
    Y: ToToFrom,
{
    type Res = <Y as ToToFrom>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes> EuImplTrait
    for EuImpl<K, S, U, Y, F>
where
    _EuHelp: _EuHelpTrait<K, S, U>,
    Pred<K, Y>: PreExistTrait,
    <_EuHelp as _EuHelpTrait<K, S, U>>::SatS: Intersect<<Pred<K, Y> as PreExistTrait>::Res>,
    <<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
            <Pred<K, Y> as PreExistTrait>::Res,
        >>::Res : Clean,
    <_EuHelp as _EuHelpTrait<K, S, U>>::SatU : Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
        >>::Res as Clean>::Res>,
    <<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res : FromToTo,
    Y : SfddEq<<<<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>,
    EuImpl<
        K,
        S,
        U,
        <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res,
        <Y as SfddEq<<<<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>>::Res,
    > : EuImplTrait
{
    type Res = <EuImpl<
        K,
        S,
        U,
        <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res,
        <Y as SfddEq<<<<_EuHelp as _EuHelpTrait<K, S, U>>::SatU as Union<
            <<<_EuHelp as _EuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreExistTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>>::Res,
    > as EuImplTrait>::Res;
}

pub trait _EuHelpTrait<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> {
    type SatS: OnlyFromNodes;
    type SatU: OnlyFromNodes;
}

pub struct _EuHelp {}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> _EuHelpTrait<K, S, U> for _EuHelp
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res:
        DropToNodes,
    <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : Clean,
    <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res : OnlyFromNodes,
    K: Apply<And, U>,
    <K as Apply<And, U>>::Res: Clean,
    <<K as Apply<And, U>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res:
        DropToNodes,
    <<<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : Clean,
    <<<<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res : OnlyFromNodes
{
    type SatS =
        <<<<<K as Apply<And, S >>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res ;
    type SatU =
        <<<<<K as Apply<And, U >>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res ;
}

#[allow(dead_code)]
fn test_eu() {
    use crate::nat::*;
    type K1 = kripke!(
        (P0) -> (P1),
        (P1) -> (P2),
        (P2) -> (P0)
    );

    type S1 = enc_sfdd!((Fr<P0>));
    type U1 = enc_sfdd!((Fr<P1>));

    type ExpectedS1EuU1 = enc_sfdd!((Fr<P0>), (Fr<P1>));
    type S1EuU1 = <Ctl as Eu<K1, S1, U1>>::Res;
    assert_type_eq!(ExpectedS1EuU1, S1EuU1);

    type K2 = kripke!(
        (P0) -> (P1),
        (P1) -> (P0),
        (P1) -> (P2),
        (P2) -> (P3),
        (P3) -> (P4),
        (P4) -> (P3)
    );

    type S2 = enc_sfdd!((Fr<P0>), (Fr<P1>));
    type U2 = enc_sfdd!((Fr<P2>));

    type ExpectedS2EuU2 = enc_sfdd!((Fr<P0>), (Fr<P1>), (Fr<P2>));
    type S2EuU2 = <Ctl as Eu<K2, S2, U2>>::Res;
    assert_type_eq!(ExpectedS2EuU2, S2EuU2);

    type K3 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1),
        (P1) -> (P6),
        (P6) -> (P1, P2),
        (P1, P2) -> (P4),
        (P4) -> (P4, P5),
        (P4, P5) -> (P4),
    );

    type S3 = enc_sfdd!((Fr<P1>));
    type U3 = enc_sfdd!((Fr<P4>));
    type ExpectedS3EuU3 = enc_sfdd!(
        (Fr<P1>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P4>),
        (Fr<P4>, Fr<P5>)
    );
    type S3EuU3 = <Ctl as Eu<K3, S3, U3>>::Res;
    assert_type_eq!(ExpectedS3EuU3, S3EuU3);

    type S4 = enc_sfdd!((Fr<P1>));
    type U4 = enc_sfdd!((Fr<P6>));
    type ExpectedS4EuU4 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>), (Fr<P6>),);
    type S4EuU4 = <Ctl as Eu<K3, S4, U4>>::Res;
    assert_type_eq!(ExpectedS4EuU4, S4EuU4);

    type S5 = enc_sfdd!(());
    type U5 = enc_sfdd!((Fr<P1>, Fr<P6>));
    type ExpectedS5EuU5 = enc_sfdd!();
    type S5EuU5 = <Ctl as Eu<K3, S5, U5>>::Res;
    assert_type_eq!(ExpectedS5EuU5, S5EuU5);
}

#[allow(dead_code)]
fn test_eu2() {
    use crate::nat::*;

    type K1 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P4),
        (P4) -> (P5),
        (P5) -> (P4),
        (P1, P2) -> (P5)
    );

    type S1 = enc_sfdd!((Fr<P1>));
    type U1 = enc_sfdd!((Fr<P4>));
    type ExpectedS1EuU1 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>), (Fr<P4>));
    type S1EuU1 = <Ctl as Eu<K1, S1, U1>>::Res;
    assert_type_eq!(ExpectedS1EuU1, S1EuU1);
}
