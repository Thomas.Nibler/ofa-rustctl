use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::{Bool, F, T},
    ctl::Ctl,
    kripke,
    kripke_state::Fr,
    predecessor::{PreExistTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes, OnlyToNodes, ToToFrom},
};

pub trait Eg<K: Sfdd, S: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes> Eg<K, S> for Ctl
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res: DropToNodes,
    <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : FromToTo,
    EgImpl<
        K,
        S,
        <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as FromToTo>::Res,
        F,
    >: EgImplTrait,
{
    type Res = <EgImpl<
        K,
        S,
        <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as FromToTo>::Res,
        F,
    > as EgImplTrait>::Res;
}

pub struct EgImpl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Y>,
    PhantomData<Stop>,
);

pub trait EgImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

pub struct _EgImplHelp<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Y>,
);

pub trait _EgImplTrait1 {
    type _PreE: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> _EgImplTrait1 for _EgImplHelp<K, S, Y>
where
    Pred<K, Y>: PreExistTrait,
    <Pred<K, Y> as PreExistTrait>::Res: Clean,
    <<Pred<K, Y> as PreExistTrait>::Res as Clean>::Res: OnlyFromNodes,
{
    type _PreE = <<Pred<K, Y> as PreExistTrait>::Res as Clean>::Res;
}

pub trait _EgImplTrait2 {
    type _FilteredPre: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> _EgImplTrait2 for _EgImplHelp<K, S, Y>
where
    _EgImplHelp<K, S, Y>: _EgImplTrait1,
    <_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE: Apply<And, S>,
    <<_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE as Apply<And, S>>::Res:
        Intersect<<_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE>,
    <<<_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
        <_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE,
    >>::Res: Clean,
    <<<<_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
        <_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE,
    >>::Res as Clean>::Res: OnlyFromNodes,
{
    type _FilteredPre =
        <<<<_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
            <_EgImplHelp<K, S, Y> as _EgImplTrait1>::_PreE,
        >>::Res as Clean>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> EgImplTrait for EgImpl<K, S, Y, F>
where
    _EgImplHelp<K, S, Y>: _EgImplTrait2,
    <_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre: FromToTo,
    <<_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre as FromToTo>::Res : SfddEq<
            Y,
        >,
    EgImpl<
            K,
            S,
            <<_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre as FromToTo>::Res,
            <<<_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre as FromToTo>::Res as SfddEq<
                Y,
            >>::Res,
        > : EgImplTrait
{
    type Res =
        <EgImpl<
            K,
            S,
            <<_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre as FromToTo>::Res,
            <<<_EgImplHelp<K, S, Y> as _EgImplTrait2>::_FilteredPre as FromToTo>::Res as SfddEq<
                Y,
            >>::Res,
        > as EgImplTrait>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> EgImplTrait for EgImpl<K, S, Y, T>
where
    Y: ToToFrom,
{
    type Res = <Y as ToToFrom>::Res;
}

#[allow(dead_code)]
fn test_eg() {
    use crate::nat::*;
    type K1 = kripke!(
        (P0) -> (P1),
        (P0) -> (P2),
        (P1) -> (P2),
        (P2) -> (P1),
        (P2) -> (P0),
        (P3) -> (P1),
        (P4) -> (P3),
        (P5) -> (P4),
        (P6) -> (P5)
    );

    type E1 = single_set_sfdd!(Fr<P1>);

    type EgE1 = <Ctl as Eg<K1, E1>>::Res;
    type ExpectedEgE1 = enc_sfdd!();
    assert_type_eq!(ExpectedEgE1, EgE1);

    type E12 = enc_sfdd!((Fr<P1>), (Fr<P2>));
    type ExpectedEgE12 = enc_sfdd!((Fr<P1>), (Fr<P2>));
    type EgE12 = <Ctl as Eg<K1, E12>>::Res;
    assert_type_eq!(ExpectedEgE12, EgE12);

    type E012 = enc_sfdd!((Fr<P0>), (Fr<P1>), (Fr<P2>));
    type ExpectedEgE012 = enc_sfdd!((Fr<P0>), (Fr<P1>), (Fr<P2>));
    type EgE012 = <Ctl as Eg<K1, E012>>::Res;
    assert_type_eq!(ExpectedEgE012, EgE012);
}

#[allow(dead_code)]
fn test_eg2() {
    use crate::nat::*;

    type K1 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1)
    );
    type ExpectedEg12 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>));
    type E12 = enc_sfdd!((Fr<P1>), (Fr<P2>));
    type Eg12 = <Ctl as Eg<K1, E12>>::Res;
    assert_type_eq!(ExpectedEg12, Eg12);

    type ExpectedEg1 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>));
    type E1 = enc_sfdd!((Fr<P1>));
    type Eg1 = <Ctl as Eg<K1, E1>>::Res;
    assert_type_eq!(ExpectedEg1, Eg1);
}

#[allow(dead_code)]
fn test_eg3() {
    use crate::nat::*;

    type K2 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1),
        (P1, P3) -> (P1, P4),
        (P1, P4) -> (P1),
        (P1, P4) -> (P5),
        (P5) -> (P1, P4)
    );

    /*                  |-----V
     * 1 -> 12 -> 13 -> 14 <- 23
     * ^----------|     |
     * ^----------------|
     * */

    type E123 = enc_sfdd!((Fr<P1>), (Fr<P2>), (Fr<P3>));
    type ExpectedEg123 = enc_sfdd!(
        (Fr<P1>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P1>, Fr<P4>)
    );
    type Eg123 = <Ctl as Eg<K2, E123>>::Res;
    assert_type_eq!(ExpectedEg123, Eg123);

    type E1 = enc_sfdd!((Fr<P1>));
    type ExpectedEg1 = enc_sfdd!(
        (Fr<P1>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P1>, Fr<P4>)
    );
    type Eg1 = <Ctl as Eg<K2, E1>>::Res;
    assert_type_eq!(ExpectedEg1, Eg1);

    type E1and23 = <enc_sfdd!((Fr<P1>)) as Apply<And, enc_sfdd!((Fr<P2>), (Fr<P3>))>>::Res;
    type ExpectedEg1and23 = enc_sfdd!();
    type Eg1and23 = <Ctl as Eg<K2, E1and23>>::Res;
    assert_type_eq!(ExpectedEg1and23, Eg1and23);

    // same as K2 but with 12 <- 13 added
    type K3 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1, P2),
        (P1, P3) -> (P1),
        // (P1, P3) -> (P1, P4),
        // (P1, P4) -> (P1),
        // (P1, P4) -> (P5),
        // (P5) -> (P1, P4)
    );

    /*       V----|     |-----V
     * 1 -> 12 -> 13 -> 14 <- 23
     * ^----------|     |
     * ^----------------|
     * */
    type K3Eg1and23 = <Ctl as Eg<K3, E1and23>>::Res;
    type K3ExpectedEg1and23 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    assert_type_eq!(K3ExpectedEg1and23, K3Eg1and23);
}

#[allow(dead_code)]
fn test_eg4() {
    use crate::nat::*;
    type K3 = kripke!(
        (P0) -> (P1),
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1, P2),
        (P1, P3) -> (P1)
    );
    type E1and23 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    type K3Eg1and23 = <Ctl as Eg<K3, E1and23>>::Res;
    type K3ExpectedEg1and23 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    assert_type_eq!(K3ExpectedEg1and23, K3Eg1and23);
}
