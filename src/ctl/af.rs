use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::{Bool, F, T},
    ctl::Ctl,
    predecessor::{PreAllTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq, Union},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes},
};

pub trait Af<K: Sfdd, S: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes> Af<K, S> for Ctl
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res: DropToNodes,
    AfImpl<
        K,
        <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res,
        F,
    >: AfImplTrait,
{
    type Res = <AfImpl<
        K,
        <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res,
        F,
    > as AfImplTrait>::Res;
}

pub struct AfImpl<K: Sfdd, S: OnlyFromNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Stop>,
);

pub trait AfImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes + FromToTo> AfImplTrait for AfImpl<K, S, F>
where
    Pred<K, <S as FromToTo>::Res>: PreAllTrait,
    <Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res: Clean,
    <<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res: Union<S>,
    <<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res: Clean,
    <<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res: OnlyFromNodes,
    S : SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>,
AfImpl<
        K,
        <<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res,
        <S as SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>>::Res,
    > : AfImplTrait

{
    type Res = <AfImpl<
        K,
        <<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res,
        <S as SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreAllTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>>::Res,
    > as AfImplTrait>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes> AfImplTrait for AfImpl<K, S, T> {
    type Res = S;
}

#[allow(dead_code)]
fn test_af() {
    use crate::kripke;
    use crate::kripke_state::*;
    use crate::nat::*;

    type K1 = kripke!(
        (P1, P4) -> (P1, P2, P4),
        (P1, P2, P4) -> (P1, P4),
        (P5, P4) -> (P1, P4),

        (P3, P5) -> (P5, P4),
        (P3, P5) -> (P2, P5),
        (P2, P5) -> (P3, P5),

        (P6) -> (P1, P4),
        (P6) -> (P7),
        (P7) -> (P6),
    );

    type S1 = enc_sfdd!((Fr<P1>));
    type ExpectedAfS1 = enc_sfdd!((Fr<P1>, Fr<P4>), (Fr<P1>, Fr<P2>, Fr<P4>), (Fr<P5>, Fr<P4>),);
    type AfS1 = <Ctl as Af<K1, S1>>::Res;
    assert_type_eq!(ExpectedAfS1, AfS1);

    type S2 = enc_sfdd!((Fr<P1>), (Fr<P2>));
    type ExpectedAfS2 = enc_sfdd!(
        (Fr<P1>, Fr<P4>),
        (Fr<P1>, Fr<P2>, Fr<P4>),
        (Fr<P5>, Fr<P4>),
        (Fr<P3>, Fr<P5>),
        (Fr<P2>, Fr<P5>)
    );
    type AfS2 = <Ctl as Af<K1, S2>>::Res;
    assert_type_eq!(ExpectedAfS2, AfS2);
}
