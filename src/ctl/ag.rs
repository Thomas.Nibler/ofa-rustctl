use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::{Bool, F, T},
    ctl::Ctl,
    predecessor::{PreAllTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes, OnlyToNodes, ToToFrom},
};

pub trait Ag<K: Sfdd, S: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes> Ag<K, S> for Ctl
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res: DropToNodes,
    <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : FromToTo,
    AgImpl<
        K,
        S,
        <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as FromToTo>::Res,
        F,
    >: AgImplTrait,
{
    type Res = <AgImpl<
        K,
        S,
        <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as FromToTo>::Res,
        F,
    > as AgImplTrait>::Res;
}

pub struct AgImpl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Y>,
    PhantomData<Stop>,
);

pub trait AgImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

pub struct _AgImplHelp<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Y>,
);

pub trait _AgImplTrait1 {
    type _PreE: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> _AgImplTrait1 for _AgImplHelp<K, S, Y>
where
    Pred<K, Y>: PreAllTrait,
    <Pred<K, Y> as PreAllTrait>::Res: Clean,
    <<Pred<K, Y> as PreAllTrait>::Res as Clean>::Res: OnlyFromNodes,
{
    type _PreE = <<Pred<K, Y> as PreAllTrait>::Res as Clean>::Res;
}

pub trait _AgImplTrait2 {
    type _FilteredPre: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> _AgImplTrait2 for _AgImplHelp<K, S, Y>
where
    _AgImplHelp<K, S, Y>: _AgImplTrait1,
    <_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE: Apply<And, S>,
    <<_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE as Apply<And, S>>::Res:
        Intersect<<_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE>,
    <<<_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
        <_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE,
    >>::Res: Clean,
    <<<<_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
        <_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE,
    >>::Res as Clean>::Res: OnlyFromNodes,
{
    type _FilteredPre =
        <<<<_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE as Apply<And, S>>::Res as Intersect<
            <_AgImplHelp<K, S, Y> as _AgImplTrait1>::_PreE,
        >>::Res as Clean>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> AgImplTrait for AgImpl<K, S, Y, F>
where
    _AgImplHelp<K, S, Y>: _AgImplTrait2,
    <_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre: FromToTo,
    <<_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre as FromToTo>::Res : SfddEq<
            Y,
        >,
    AgImpl<
            K,
            S,
            <<_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre as FromToTo>::Res,
            <<<_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre as FromToTo>::Res as SfddEq<
                Y,
            >>::Res,
        > : AgImplTrait
{
    type Res =
        <AgImpl<
            K,
            S,
            <<_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre as FromToTo>::Res,
            <<<_AgImplHelp<K, S, Y> as _AgImplTrait2>::_FilteredPre as FromToTo>::Res as SfddEq<
                Y,
            >>::Res,
        > as AgImplTrait>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, Y: OnlyToNodes> AgImplTrait for AgImpl<K, S, Y, T>
where
    Y: ToToFrom,
{
    type Res = <Y as ToToFrom>::Res;
}

#[allow(dead_code)]
fn test_ag() {
    use crate::kripke;
    use crate::kripke_state::*;
    use crate::nat::*;

    type K1 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1),
        (P4) -> (P1, P2),
        (P4) -> (P4, P5),
        (P4, P5) -> (P4),
    );

    type S1 = enc_sfdd!((Fr<P1>));
    type ExpectedAgS1 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    type AgS1 = <Ctl as Ag<K1, S1>>::Res;
    assert_type_eq!(ExpectedAgS1, AgS1);

    type S2 = enc_sfdd!((Fr<P1>), (Fr<P4>));
    type ExpectedAgS2 = enc_sfdd!(
        (Fr<P1>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P4>),
        (Fr<P4>),
        (Fr<P4>, Fr<P5>),
    );
    type AgS2 = <Ctl as Ag<K1, S2>>::Res;
    assert_type_eq!(ExpectedAgS2, AgS2);

    type K2 = kripke!(
        (P1, P4) -> (P1, P2, P4),
        (P1, P2, P4) -> (P1, P4),
        (P5, P4) -> (P1, P4),
        (P5, P4) -> (P2, P5),
        (P3, P5) -> (P5, P4),
        (P3, P5) -> (P2, P5),
        (P2, P5) -> (P3, P5),
    );

    type S3 = enc_sfdd!((Fr<P1>));
    type ExpectedAgS3 = enc_sfdd!((Fr<P1>, Fr<P4>), (Fr<P1>, Fr<P2>, Fr<P4>));
    type AgS3 = <Ctl as Ag<K2, S3>>::Res;
    assert_type_eq!(ExpectedAgS3, AgS3);

    type S4 = enc_sfdd!((Fr<P1>), (Fr<P4>));
    type ExpectedAgS4 = enc_sfdd!((Fr<P1>, Fr<P4>), (Fr<P1>, Fr<P2>, Fr<P4>), (Fr<P5>, Fr<P4>));
    type AgS4 = <Ctl as Ag<K2, S4>>::Res;

    type S5 = enc_sfdd!((Fr<P1>), (Fr<P5>));
    type ExpectedAgS5 = enc_sfdd!(
        (Fr<P1>, Fr<P4>),
        (Fr<P1>, Fr<P2>, Fr<P4>),
        (Fr<P5>, Fr<P4>),
        (Fr<P5>, Fr<P4>),
        (Fr<P3>, Fr<P5>),
        (Fr<P2>, Fr<P5>),
    );
    type AgS5 = <Ctl as Ag<K2, S5>>::Res;
    assert_type_eq!(ExpectedAgS5, AgS5);
}
