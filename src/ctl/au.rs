use std::marker::PhantomData;

use crate::{
    assert_type_eq,
    boolean::{Bool, F, T},
    kripke,
    predecessor::{PreAllTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq, Union},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes, OnlyToNodes, ToToFrom},
};

use super::Ctl;

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> Au<K, S, U> for Ctl
where
    _AuHelp: _AuHelpTrait<K, S, U>,
    AuImpl<K, S, U, crate::sfdd::Bottom, F>: AuImplTrait,
{
    type Res = <AuImpl<K, S, U, crate::sfdd::Bottom, F> as AuImplTrait>::Res;
}

pub trait Au<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

pub struct AuImpl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<U>,
    PhantomData<Y>,
    PhantomData<Stop>,
);

pub trait AuImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes> AuImplTrait
    for AuImpl<K, S, U, Y, T>
where
    Y: ToToFrom,
{
    type Res = <Y as ToToFrom>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes, Y: OnlyToNodes> AuImplTrait
    for AuImpl<K, S, U, Y, F>
where
    _AuHelp: _AuHelpTrait<K, S, U>,
    Pred<K, Y>: PreAllTrait,
    <_AuHelp as _AuHelpTrait<K, S, U>>::SatS: Intersect<<Pred<K, Y> as PreAllTrait>::Res>,
    <<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
            <Pred<K, Y> as PreAllTrait>::Res,
        >>::Res : Clean,
    <_AuHelp as _AuHelpTrait<K, S, U>>::SatU : Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
        >>::Res as Clean>::Res>,
    <<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res : FromToTo,
    Y : SfddEq<<<<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>,
    AuImpl<
        K,
        S,
        U,
        <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res,
        <Y as SfddEq<<<<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>>::Res,
    > : AuImplTrait
{
    type Res = <AuImpl<
        K,
        S,
        U,
        <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res,
        <Y as SfddEq<<<<_AuHelp as _AuHelpTrait<K, S, U>>::SatU as Union<
            <<<_AuHelp as _AuHelpTrait<K, S, U>>::SatS as Intersect<
                <Pred<K, Y> as PreAllTrait>::Res,
            >>::Res as Clean>::Res,
        >>::Res as FromToTo>::Res>>::Res,
    > as AuImplTrait>::Res;
}

pub trait _AuHelpTrait<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> {
    type SatS: OnlyFromNodes;
    type SatU: OnlyFromNodes;
}

pub struct _AuHelp {}

impl<K: Sfdd, S: OnlyFromNodes, U: OnlyFromNodes> _AuHelpTrait<K, S, U> for _AuHelp
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res:
        DropToNodes,
    <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : Clean,
    <<<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res : OnlyFromNodes,
    K: Apply<And, U>,
    <K as Apply<And, U>>::Res: Clean,
    <<K as Apply<And, U>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res:
        DropToNodes,
    <<<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res : Clean,
    <<<<<K as Apply<And, U>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res : OnlyFromNodes
{
    type SatS =
        <<<<<K as Apply<And, S >>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res ;
    type SatU =
        <<<<<K as Apply<And, U >>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res as Clean>::Res ;
}

#[allow(dead_code)]
fn test_au() {
    use crate::kripke_state::*;
    use crate::nat::*;

    type K1 = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P4),
        (P4) -> (P5),
        (P5) -> (P4),
        (P1, P2) -> (P5)
    );

    type S1 = enc_sfdd!((Fr<P1>));
    type U1 = enc_sfdd!((Fr<P4>));
    type ExpectedS1AuU1 = enc_sfdd!((Fr<P1>, Fr<P3>), (Fr<P4>));
    type S1AuU1 = <Ctl as Au<K1, S1, U1>>::Res;
    assert_type_eq!(ExpectedS1AuU1, S1AuU1);

    type U2 = enc_sfdd!((Fr<P4>), (Fr<P5>));
    type ExpectedS1AuU2 = enc_sfdd!(
        (Fr<P1>),
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P4>),
        (Fr<P5>)
    );
    type S1AuU2 = <Ctl as Au<K1, S1, U2>>::Res;
    assert_type_eq!(ExpectedS1AuU2, S1AuU2);
}
