use std::marker::PhantomData;

use crate::{
    boolean::{Bool, F, T},
    ctl::Ctl,
    kripke,
    kripke_state::Fr,
    predecessor::{PreExistTrait, Pred},
    sfdd::{And, Apply, Clean, Intersect, Sfdd, SfddEq, Union},
    sfdd_state_utils::{DropToNodes, FromToTo, OnlyFromNodes},
};

pub trait Ef<K: Sfdd, S: OnlyFromNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes> Ef<K, S> for Ctl
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Clean,
    <<K as Apply<And, S>>::Res as Clean>::Res: Intersect<K>,
    <<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res: DropToNodes,
    EfImpl<
        K,
        <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res,
        F,
    >: EfImplTrait,
{
    type Res = <EfImpl<
        K,
        <<<<K as Apply<And, S>>::Res as Clean>::Res as Intersect<K>>::Res as DropToNodes>::Res,
        F,
    > as EfImplTrait>::Res;
}

pub struct EfImpl<K: Sfdd, S: OnlyFromNodes, Stop: Bool>(
    PhantomData<K>,
    PhantomData<S>,
    PhantomData<Stop>,
);

pub trait EfImplTrait {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: OnlyFromNodes + FromToTo> EfImplTrait for EfImpl<K, S, F>
where
    Pred<K, <S as FromToTo>::Res>: PreExistTrait,
    <Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res: Clean,
    <<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res  :Union<S>,
    <<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res  as Union<S>>::Res: Clean,
    <<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res  as Union<S>>::Res as Clean>::Res: OnlyFromNodes,
    S : SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>,
EfImpl<
        K,
        <<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res,
        <S as SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>>::Res,
    > : EfImplTrait

{
    type Res = <EfImpl<
        K,
        <<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res,
        <S as SfddEq<<<<<Pred<K, <S as FromToTo>::Res> as PreExistTrait>::Res as Clean>::Res as Union<S>>::Res as Clean>::Res>>::Res,
    > as EfImplTrait>::Res;
}

impl<K: Sfdd, S: OnlyFromNodes> EfImplTrait for EfImpl<K, S, T> {
    type Res = S;
}

#[allow(dead_code)]
fn test_ef() {
    use crate::assert_type_eq;
    use crate::nat::*;
    type K1 = kripke!(
        (P0) -> (P1),
        (P0) -> (P2),
        (P1) -> (P2),
        (P2) -> (P1),
        (P3) -> (P1),
        (P4) -> (P3),
        (P5) -> (P4),
        (P6) -> (P5)
    );

    type E1 = single_set_sfdd!(Fr<P1>);

    type EfE1 = <crate::ctl::Ctl as crate::ctl::Ef<K1, E1>>::Res;
    type ExpectedEfE1 = enc_sfdd!(
        (Fr<P0>),
        (Fr<P1>),
        (Fr<P2>),
        (Fr<P3>),
        (Fr<P4>),
        (Fr<P5>),
        (Fr<P6>)
    );
    assert_type_eq!(ExpectedEfE1, EfE1);

    type E3 = single_set_sfdd!(Fr<P3>);
    type EfE3 = <Ctl as Ef<K1, E3>>::Res;
    type ExpectedEfE3 = enc_sfdd!((Fr<P6>), (Fr<P5>), (Fr<P4>), (Fr<P3>));
    assert_type_eq!(ExpectedEfE3, EfE3);

    type K2 = kripke!(
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1, P2),
        (P1, P3) -> (P4),
        (P4) -> (P2, P3),
        (P2, P3) -> (P2, P4),
        (P2, P4) -> (P4),
        (P4) -> (P2, P4),
    );

    type S1 = enc_sfdd!((Fr<P4>));
    // all states reach P4
    type ExpectedEfS1 = enc_sfdd!(
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P4>),
        (Fr<P2>, Fr<P3>),
        (Fr<P2>, Fr<P4>),
        (Fr<P4>),
    );
    type EfS1 = <Ctl as Ef<K2, S1>>::Res;
    assert_type_eq!(ExpectedEfS1, EfS1);

    type S2 = enc_sfdd!((Fr<P1>));
    type ExpectedEfS2 = enc_sfdd!((Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>),);
    type EfS2 = <Ctl as Ef<K2, S2>>::Res;
    assert_type_eq!(ExpectedEfS2, EfS2);

    type S3 = enc_sfdd!((Fr<P2>, Fr<P3>), (Fr<P1>, Fr<P2>));
    // all states reach S3
    type ExpectedEfS3 = enc_sfdd!(
        (Fr<P1>, Fr<P2>),
        (Fr<P1>, Fr<P3>),
        (Fr<P4>),
        (Fr<P2>, Fr<P3>),
        (Fr<P2>, Fr<P4>),
        (Fr<P4>),
    );
    type EfS3 = <Ctl as Ef<K2, S3>>::Res;
    assert_type_eq!(ExpectedEfS3, EfS3);

    type S4 = enc_sfdd!((Fr<P1>, Fr<P2>, Fr<P3>));
    type ExpectedEfS4 = enc_sfdd!();
    type EfS4 = <Ctl as Ef<K2, S4>>::Res;
    assert_type_eq!(ExpectedEfS4, EfS4);
}
