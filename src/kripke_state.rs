use std::marker::PhantomData;

use crate::assert_type_eq;
use crate::boolean::*;
use crate::nat::*;
use crate::sfdd::Sfdd;
use crate::sfdd::SfddLabel;
use crate::sfdd_state_utils::OnlyFromNodes;
use crate::tools::runtime_sfdd;

pub struct Kripke<S: Sfdd + OnlyFromNodes, T: Sfdd>(PhantomData<S>, PhantomData<T>);

pub trait Compare<S> {
    type Lt: Bool;
    type Eq: Bool;
    type Gt: Bool;
}

pub struct Fr<S: Nat> {
    _s: PhantomData<S>,
}

pub struct To<S: Nat> {
    _s: PhantomData<S>,
}

impl<F1: Nat, F2: Nat> Compare<Fr<F1>> for Fr<F2> {
    type Lt = <F2 as Nat>::Lt<F1>;
    type Eq = <F2 as Nat>::Eq<F1>;
    type Gt = <F2 as Nat>::Gt<F1>;
}

impl<T1: Nat, T2: Nat> Compare<To<T1>> for To<T2> {
    type Lt = <T2 as Nat>::Lt<T1>;
    type Eq = <T2 as Nat>::Eq<T1>;
    type Gt = <T2 as Nat>::Gt<T1>;
}

impl<F1: Nat, T1: Nat> Compare<To<T1>> for Fr<F1> {
    type Lt = <<F1 as Nat>::Lt<T1> as Bool>::Or<<F1 as Nat>::Eq<T1>>;
    type Eq = F;
    type Gt = <F1 as Nat>::Gt<T1>;
}

impl<F1: Nat, T1: Nat> Compare<Fr<F1>> for To<T1> {
    type Lt = <Fr<F1> as Compare<To<T1>>>::Gt;
    type Eq = F;
    type Gt = <Fr<F1> as Compare<To<T1>>>::Lt;
}

impl<N: Nat> SfddLabel for Fr<N> {
    fn reify() -> crate::tools::runtime_sfdd::El {
        runtime_sfdd::El::Fr(N::REIFY)
    }
}

impl<N: Nat> SfddLabel for To<N> {
    fn reify() -> runtime_sfdd::El {
        runtime_sfdd::El::To(N::REIFY)
    }
}

#[allow(dead_code)]
fn test_compare_siblings() {
    type F2 = Fr<P2>;
    type T2 = To<P2>;
    type F3 = Fr<P3>;

    assert_type_eq!(<F2 as Compare<T2>>::Eq, F);
    assert_type_eq!(<F2 as Compare<T2>>::Lt, T);
    assert_type_eq!(<F2 as Compare<T2>>::Gt, F);
    assert_type_eq!(<T2 as Compare<F2>>::Eq, F);
    assert_type_eq!(<T2 as Compare<F2>>::Lt, F);
    assert_type_eq!(<T2 as Compare<F2>>::Gt, T);

    assert_type_eq!(<F3 as Compare<T2>>::Eq, F);
    assert_type_eq!(<F3 as Compare<T2>>::Lt, F);
    assert_type_eq!(<F3 as Compare<T2>>::Gt, T);

    assert_type_eq!(<T2 as Compare<F2>>::Eq, F);
    assert_type_eq!(<T2 as Compare<F2>>::Lt, F);
    assert_type_eq!(<T2 as Compare<F2>>::Gt, T);

    assert_type_eq!(<Fr<P6> as Compare<F2>>::Gt, T);
}
