use crate::assert_type_eq;

pub struct T {}
pub struct F {}

pub trait True: Bool {}
pub trait False: Bool {}

impl True for T {}
impl False for F {}

pub trait Bool {
    type Not: Bool;
    type And<B: Bool>: Bool;
    type Or<B: Bool>: Bool;
    type Eq<B: Bool>: Bool;
}

impl Bool for T {
    type Not = F;
    type And<B: Bool> = B;
    type Or<B: Bool> = T;
    type Eq<B: Bool> = B;
}

impl Bool for F {
    type Not = T;
    type And<B: Bool> = F;
    type Or<B: Bool> = B;
    type Eq<B: Bool> = B::Not;
}

#[allow(dead_code)]
fn a() {
    assert_type_eq!(F, <T as Bool>::Not);
    assert_type_eq!(T, <F as Bool>::Not);

    assert_type_eq!(F, <F as Bool>::And<F>);
    assert_type_eq!(F, <T as Bool>::And<F>);
    assert_type_eq!(F, <F as Bool>::And<T>);
    assert_type_eq!(T, <T as Bool>::And<T>);

    assert_type_eq!(F, <F as Bool>::Or<F>);
    assert_type_eq!(T, <T as Bool>::Or<F>);
    assert_type_eq!(T, <F as Bool>::Or<T>);
    assert_type_eq!(T, <T as Bool>::Or<T>);

    assert_type_eq!(T, <F as Bool>::Eq<F>);
    assert_type_eq!(F, <T as Bool>::Eq<F>);
    assert_type_eq!(F, <F as Bool>::Eq<T>);
    assert_type_eq!(T, <T as Bool>::Eq<T>);
}
