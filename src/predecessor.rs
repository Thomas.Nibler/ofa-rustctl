use std::marker::PhantomData;

use crate::kripke_state::{Fr, To};
use crate::sfdd::{And, AndNot, Apply, Bottom, Clean, Intersect, Sfdd};
use crate::sfdd_state_utils::{DropToNodes, OnlyFromNodes, OnlyToNodes};
use crate::{assert_type_eq, kripke};

pub trait PreExistTrait {
    type Res: Sfdd + OnlyFromNodes;
}

pub struct Pred<K: Sfdd, S: Sfdd + OnlyToNodes>(PhantomData<K>, PhantomData<S>);

impl<K: Sfdd, S: Sfdd + OnlyToNodes> PreExistTrait for Pred<K, S>
where
    K: Apply<And, S>,
    <K as Apply<And, S>>::Res: Intersect<K>,
    <<K as Apply<And, S>>::Res as Intersect<K>>::Res: DropToNodes,
{
    type Res = <<<K as Apply<And, S>>::Res as Intersect<K>>::Res as DropToNodes>::Res;
}

#[allow(dead_code)]
fn test_pre_exist() {
    use crate::nat::*;

    type K1 = kripke!(
        (P0) -> (P1),
        (P0) -> (P2),
        (P1) -> (P2),
        (P2) -> (P1),
        (P3) -> (P1)
    );

    type S1 = enc_sfdd!((To<P1>));
    type PreES1 = <<Pred<K1, S1> as PreExistTrait>::Res as Clean>::Res;
    type ExpectedPreES1 = enc_sfdd!((Fr<P0>), (Fr<P2>), (Fr<P3>));
    assert_type_eq!(ExpectedPreES1, PreES1);

    type S2 = enc_sfdd!((To<P3>), (To<P0>));
    type PreES2 = <<Pred<K1, S2> as PreExistTrait>::Res as Clean>::Res;
    type ExpectedPreES2 = Bottom;
    assert_type_eq!(ExpectedPreES2, PreES2);

    type S3 = enc_sfdd!((To<P2>));
    type PreES3 = <<Pred<K1, S3> as PreExistTrait>::Res as Clean>::Res;
    type ExpectedPreES3 = enc_sfdd!((Fr<P0>), (Fr<P1>));
    assert_type_eq!(ExpectedPreES3, PreES3);

    type AllOfK1 = enc_sfdd!((To<P0>), (To<P1>), (To<P2>), (To<P3>));
    type PreEAllOfK1 = <Pred<K1, AllOfK1> as PreExistTrait>::Res;
    type ExpectedAllOfK1 = enc_sfdd!((Fr<P0>), (Fr<P1>), (Fr<P2>), (Fr<P3>));
    assert_type_eq!(ExpectedAllOfK1, PreEAllOfK1);
}

#[allow(dead_code)]
fn test_pre_exist2() {
    use crate::nat::*;
    type K = kripke!(
        (P1) -> (P1, P2),
        (P1, P2) -> (P1),
        (P1, P2) -> (P1, P3),
        (P1, P3) -> (P1, P2)
    );
    type E1312 = enc_sfdd!((To<P1>, To<P2>), (To<P1>, To<P3>));
    type ExpectedPreE1312 = enc_sfdd!((Fr<P1>), (Fr<P1>, Fr<P2>), (Fr<P1>, Fr<P3>));
    type PreE1312 = <<Pred<K, E1312> as PreExistTrait>::Res as Clean>::Res;
    assert_type_eq!(ExpectedPreE1312, PreE1312);
}

pub trait PreAllTrait {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> PreAllTrait for Pred<K, S>
where
    K: DropToNodes,
    PreAllImpl<K, S>: _PreExS<K, S>
        + _TsFromPredsS<K, S>
        + _AllowedTsFromPredsS<K, S>
        + _BadTsFromPredsS<K, S>
        + _StatesToSubtract<K, S>,

    <PreAllImpl<K, S> as _PreExS<K, S>>::Res:
        Apply<AndNot, <PreAllImpl<K, S> as _StatesToSubtract<K, S>>::Res>,

    <<PreAllImpl<K, S> as _PreExS<K, S>>::Res as Apply<
        AndNot,
        <PreAllImpl<K, S> as _StatesToSubtract<K, S>>::Res,
    >>::Res: Intersect<<K as DropToNodes>::Res>,
    <<<PreAllImpl<K, S> as _PreExS<K, S>>::Res as Apply<
        AndNot,
        <PreAllImpl<K, S> as _StatesToSubtract<K, S>>::Res,
    >>::Res as Intersect<<K as DropToNodes>::Res>>::Res: Clean,
    <<<<PreAllImpl<K, S> as _PreExS<K, S>>::Res as Apply<
        AndNot,
        <PreAllImpl<K, S> as _StatesToSubtract<K, S>>::Res,
    >>::Res as Intersect<<K as DropToNodes>::Res>>::Res as Clean>::Res: OnlyFromNodes,
{
    type Res = <<<<PreAllImpl<K, S> as _PreExS<K, S>>::Res as Apply<
        AndNot,
        <PreAllImpl<K, S> as _StatesToSubtract<K, S>>::Res,
    >>::Res as Intersect<<K as DropToNodes>::Res>>::Res as Clean>::Res;
}

pub struct PreAllImpl<K: Sfdd, S: OnlyToNodes>(PhantomData<K>, PhantomData<S>);

pub trait _PreExS<K: Sfdd, S: Sfdd + OnlyToNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> _PreExS<K, S> for PreAllImpl<K, S>
where
    Pred<K, S>: PreExistTrait,
{
    type Res = <Pred<K, S> as PreExistTrait>::Res;
}

pub trait _TsFromPredsS<K: Sfdd, S: Sfdd + OnlyToNodes> {
    type Res: Sfdd;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> _TsFromPredsS<K, S> for PreAllImpl<K, S>
where
    PreAllImpl<K, S>: _PreExS<K, S>,
    K: Apply<And, <Self as _PreExS<K, S>>::Res>,
{
    type Res = <K as Apply<And, <Self as _PreExS<K, S>>::Res>>::Res;
}

pub trait _AllowedTsFromPredsS<K: Sfdd, S: Sfdd + OnlyToNodes> {
    type Res: Sfdd;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> _AllowedTsFromPredsS<K, S> for PreAllImpl<K, S>
where
    Self: _TsFromPredsS<K, S>,
    <Self as _TsFromPredsS<K, S>>::Res: Apply<And, S>,
{
    type Res = <<Self as _TsFromPredsS<K, S>>::Res as Apply<And, S>>::Res;
}

pub trait _BadTsFromPredsS<K: Sfdd, S: Sfdd + OnlyToNodes> {
    type Res: Sfdd;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> _BadTsFromPredsS<K, S> for PreAllImpl<K, S>
where
    Self: _TsFromPredsS<K, S>,
    Self: _AllowedTsFromPredsS<K, S>,
    <Self as _TsFromPredsS<K, S>>::Res: Apply<AndNot, <Self as _AllowedTsFromPredsS<K, S>>::Res>,
    <<Self as _TsFromPredsS<K, S>>::Res as Apply<
        AndNot,
        <Self as _AllowedTsFromPredsS<K, S>>::Res,
    >>::Res: Intersect<K>,
{
    type Res = <<<Self as _TsFromPredsS<K, S>>::Res as Apply<
        AndNot,
        <Self as _AllowedTsFromPredsS<K, S>>::Res,
    >>::Res as Intersect<K>>::Res;
}

pub trait _StatesToSubtract<K: Sfdd, S: Sfdd + OnlyToNodes> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd, S: Sfdd + OnlyToNodes> _StatesToSubtract<K, S> for PreAllImpl<K, S>
where
    Self: _BadTsFromPredsS<K, S>,
    <Self as _BadTsFromPredsS<K, S>>::Res: DropToNodes,
{
    type Res = <<Self as _BadTsFromPredsS<K, S>>::Res as DropToNodes>::Res;
}

#[allow(dead_code)]
fn test_pre_all() {
    use crate::kripke_state::*;
    use crate::nat::*;

    type K = enc_sfdd!(
        (Fr<P1>, Fr<P2>, To<P3>),
        (Fr<P2>, To<P1>, To<P2>),
        (Fr<P3>, To<P1>, To<P2>),
        (Fr<P3>, To<P2>),
        (Fr<P4>, To<P3>),
        (Fr<P4>, To<P1>, To<P2>),
        (Fr<P5>, To<P4>)
    );
    type S1 = enc_sfdd!((To<P2>, To<P1>), (To<P2>));
    type ExpectedPreAS1 = enc_sfdd!((Fr<P2>), (Fr<P3>));
    // assert_type_eq!(ExpectedPreAS1, <Pred<K, S1> as PreAllTrait>::Res);

    type S2 = enc_sfdd!((To<P2>, To<P1>), (To<P3>));
    type ExpectedPreAS2 = enc_sfdd!((Fr<P2>), (Fr<P4>), (Fr<P1>, Fr<P2>));
    assert_type_eq!(ExpectedPreAS2, <Pred<K, S2> as PreAllTrait>::Res);

    type S3 = enc_sfdd!((To<P4>));
    type ExpectedPreAS3 = enc_sfdd!((Fr<P5>));
    assert_type_eq!(ExpectedPreAS3, <Pred<K, S3> as PreAllTrait>::Res);

    type S4 = enc_sfdd!((To<P5>));
    type ExpectedPreAS4 = enc_sfdd!();
    assert_type_eq!(ExpectedPreAS4, <Pred<K, S4> as PreAllTrait>::Res);

    type S5 = enc_sfdd!((To<P2>));
    type ExpectedPreAS5 = enc_sfdd!((Fr<P2>), (Fr<P3>));
    assert_type_eq!(ExpectedPreAS5, <Pred<K, S5> as PreAllTrait>::Res);
}

#[allow(dead_code)]
fn test_pre_all2() {
    use crate::nat::*;
    type K = kripke!(
        (P1) -> (P2, P3),
        (P2, P3) -> (P2, P4),
        (P2, P4) -> (P2, P3),
        (P1) -> (P2, P4),
        (P2, P4) -> (P1),
        (P3) -> (P2, P4),
        (P3) -> (P1),
        (P5) -> (P2, P4),
        (P5) -> (P2, P3),
        (P5) -> (P1),
    );

    type S1 = enc_sfdd!((To<P2>));
    type ExpectedPreAS1 = enc_sfdd!((Fr<P1>), (Fr<P2>, Fr<P3>));
    type PreAS1 = <Pred<K, S1> as PreAllTrait>::Res;
    assert_type_eq!(ExpectedPreAS1, PreAS1);

    type S2 = enc_sfdd!((To<P1>), (To<P4>));
    type ExpectedPreAS2 = enc_sfdd!((Fr<P3>), (Fr<P2>, Fr<P3>));
    type PreAS2 = <Pred<K, S2> as PreAllTrait>::Res;
    assert_type_eq!(ExpectedPreAS2, PreAS2);

    type S3 = enc_sfdd!((To<P1>));
    type ExpectedPreAS3 = enc_sfdd!();
    type PreAS3 = <Pred<K, S3> as PreAllTrait>::Res;
    assert_type_eq!(ExpectedPreAS3, PreAS3);
}
