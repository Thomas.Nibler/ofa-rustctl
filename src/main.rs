// #![recursion_limit = "256"]
use crate::predecessor::{PreExistTrait, Pred};
use crate::sfdd::*;

pub mod boolean;
pub mod kripke_state;
pub mod nat;
pub mod tools;
#[macro_use]
pub mod sfdd;
pub mod ctl;
pub mod predecessor;
pub mod sfdd_state_utils;
pub mod sorted_list;
#[macro_use]
pub mod kripke_macro;
#[macro_use]
pub mod ctl_macro;

fn main() {
    use nat::*;

    type K = kripke!(
        (P1, P4) -> (P1, P2, P4),
        (P1, P2, P4) -> (P1, P4),
        (P5, P4) -> (P1, P4),

        (P3, P5) -> (P5, P4),
        (P3, P5) -> (P2, P5),
        (P2, P5) -> (P3, P5),

        (P6) -> (P1, P4),
        (P6) -> (P7),
        (P7) -> (P6),
    );

    type S1 = state_set!((P1));
    type AGS1 = ctl!(K, AG(S1));

    type ExpectedAGS1 = state_set!((P1, P4), (P1, P2, P4));
    assert_type_eq!(AGS1, ExpectedAGS1);

    type EFAGS1 = ctl!(K, EF(AG(S1)));

    type ExpectedEFAGS1 = state_set!(
        (P1, P4),
        (P1, P2, P4),
        (P5, P4),
        (P3, P5),
        (P3, P5),
        (P2, P5),
        (P6),
        (P6),
        (P7),
    );
    assert_type_eq!(EFAGS1, ExpectedEFAGS1);


    // println!(
    //     "AG S1 {:?}",
    //     <AGS1 as crate::tools::decode_sfdd::DecodeSfdd>::decode()
    // );
    //
    // println!(
    //     "EF AG S1 {:?}",
    //     <EFAGS1 as crate::tools::decode_sfdd::DecodeSfdd>::decode()
    // );
}
