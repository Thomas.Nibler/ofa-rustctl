use crate::{
    kripke_state::{Fr, To},
    nat::Nat,
    sfdd::{Bottom, Node, Sfdd, Top, Union},
};

pub struct KripkeUtils;

/// Extracts all states (sets of propositions Pi wrapped in To<_>) from a transition Sfdd
/// Result: DropFromNodes(K) Union FromToTo(DropToNodes(K))
pub trait ToStates<K: Sfdd> {
    type Res: Sfdd + OnlyToNodes;
}

impl<K: Sfdd> ToStates<K> for KripkeUtils
where
    K: DropToNodes + DropFromNodes,
    <K as DropToNodes>::Res: FromToTo,
    <<K as DropToNodes>::Res as FromToTo>::Res: Union<<K as DropFromNodes>::Res>,
    <<<K as DropToNodes>::Res as FromToTo>::Res as Union<<K as DropFromNodes>::Res>>::Res:
        OnlyToNodes,
{
    type Res =
        <<<K as DropToNodes>::Res as FromToTo>::Res as Union<<K as DropFromNodes>::Res>>::Res;
}

// same thing as macro

/// Extracts all states (sets of propositions Pi wrapped in To<_>) from a transition Sfdd
/// Result: DropFromNodes(K) Union FromToTo(DropToNodes(K))
#[macro_export]
macro_rules! to_states {
    ($k:tt) => {
        <<<$k as DropToNodes>::Res as crate::sfdd_state_utils::FromToTo>::Res as crate::sfdd::Union<
                                                    <$k as crate::sfdd_utils::DropFromNodes>::Res,
                                                >>::Res
    };
}

/// Extracts all states (sets of propositions Pi wrapped in Fr<_>) from a transition Sfdd
/// Result: DropFromNodes(K) Union FromToTo(DropToNodes(K))
pub trait FrStates<K: Sfdd> {
    type Res: Sfdd + OnlyFromNodes;
}

impl<K: Sfdd> FrStates<K> for KripkeUtils
where
    K: DropFromNodes + DropToNodes,
    <K as DropFromNodes>::Res: ToToFrom,
    <<K as DropFromNodes>::Res as ToToFrom>::Res: Union<<K as DropToNodes>::Res>,
    <<<K as DropFromNodes>::Res as ToToFrom>::Res as Union<<K as DropToNodes>::Res>>::Res:
        OnlyFromNodes,
{
    type Res =
        <<<K as DropFromNodes>::Res as ToToFrom>::Res as Union<<K as DropToNodes>::Res>>::Res;
}

/// Marker trait implemented for Sfdd which only has To<_> (transition destinations)
/// as labels. Useful because that's what the input to PreExist should be, and with
/// this trait we can enforce that.
pub trait OnlyToNodes: Sfdd {}

impl OnlyToNodes for Top {}

impl OnlyToNodes for Bottom {}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> OnlyToNodes for Node<To<N>, Take, Skip>
where
    Take: OnlyToNodes,
    Skip: OnlyToNodes,
{
}

pub trait OnlyFromNodes: Sfdd {}

impl OnlyFromNodes for Top {}

impl OnlyFromNodes for Bottom {}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> OnlyFromNodes for Node<Fr<N>, Take, Skip>
where
    Take: OnlyFromNodes,
    Skip: OnlyFromNodes,
{
}

pub trait DropToNodes: Sfdd {
    type Res: Sfdd + OnlyFromNodes;
}

impl DropToNodes for Top {
    type Res = Top;
}

impl DropToNodes for Bottom {
    type Res = Bottom;
}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> DropToNodes for Node<Fr<N>, Take, Skip>
where
    Take: DropToNodes,
    Skip: DropToNodes,
{
    type Res = Node<Fr<N>, <Take as DropToNodes>::Res, <Skip as DropToNodes>::Res>;
}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> DropToNodes for Node<To<N>, Take, Skip>
where
    Take: DropToNodes,
    Skip: DropToNodes,
    <Take as DropToNodes>::Res: Union<<Skip as DropToNodes>::Res>,
    <<Take as DropToNodes>::Res as Union<<Skip as DropToNodes>::Res>>::Res: OnlyFromNodes,
{
    type Res = <<Take as DropToNodes>::Res as Union<<Skip as DropToNodes>::Res>>::Res;
}

pub trait DropFromNodes: Sfdd {
    type Res: Sfdd + OnlyToNodes;
}

impl DropFromNodes for Top {
    type Res = Top;
}

impl DropFromNodes for Bottom {
    type Res = Bottom;
}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> DropFromNodes for Node<To<N>, Take, Skip>
where
    Take: DropFromNodes,
    Skip: DropFromNodes,
{
    type Res = Node<To<N>, <Take as DropFromNodes>::Res, <Skip as DropFromNodes>::Res>;
}

impl<N: Nat, Take: Sfdd, Skip: Sfdd> DropFromNodes for Node<Fr<N>, Take, Skip>
where
    Take: DropFromNodes,
    Skip: DropFromNodes,
    <Take as DropFromNodes>::Res: Union<<Skip as DropFromNodes>::Res>,
    <<Take as DropFromNodes>::Res as Union<<Skip as DropFromNodes>::Res>>::Res: OnlyToNodes,
{
    type Res = <<Take as DropFromNodes>::Res as Union<<Skip as DropFromNodes>::Res>>::Res;
}

/// Convert all Fr<_> nodes in Sfdd to To<_>
/// Used to use result from PreExist (all Fr<_> nodes) as input to PreExist
pub trait FromToTo: Sfdd + OnlyFromNodes {
    type Res: Sfdd + OnlyToNodes;
}

impl FromToTo for Bottom {
    type Res = Bottom;
}

impl FromToTo for Top {
    type Res = Top;
}

impl<N: Nat, Take: FromToTo, Skip: FromToTo> FromToTo for Node<Fr<N>, Take, Skip> {
    type Res = Node<To<N>, <Take as FromToTo>::Res, <Skip as FromToTo>::Res>;
}

pub trait ToToFrom: Sfdd + OnlyToNodes {
    type Res: Sfdd + OnlyFromNodes;
}

impl ToToFrom for Bottom {
    type Res = Bottom;
}

impl ToToFrom for Top {
    type Res = Top;
}

impl<N: Nat, Take: ToToFrom, Skip: ToToFrom> ToToFrom for Node<To<N>, Take, Skip> {
    type Res = Node<Fr<N>, <Take as ToToFrom>::Res, <Skip as ToToFrom>::Res>;
}

#[test]
#[allow(dead_code)]
fn test_drop_to_nodes() {
    use crate::assert_type_eq;
    use crate::nat::*;

    type S1 = Node<Fr<P1>, Top, Bottom>;
    assert_type_eq!(S1, <S1 as DropToNodes>::Res);

    type S2 = Node<To<P1>, Top, Bottom>;
    assert_type_eq!(Top, <S2 as DropToNodes>::Res);

    type S3 = Node<To<P1>, Node<Fr<P2>, Top, Bottom>, Node<Fr<P3>, Top, Bottom>>;
    assert_type_eq!(
        Node<Fr<P2>, Top, Node<Fr<P3>, Top, Bottom>>,
        <S3 as DropToNodes>::Res
    );
}
